<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('email')->unique();
	        $table->string('password', 60);
	        $table->string('firstname');
	        $table->string('lastname');
	        $table->string('dob');
	        $table->string('phone');
	        $table->string('website');
            $table->string('avatar');
            $table->integer('role_id')->unsigned();
	        $table->rememberToken();
	        $table->timestamps();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
