<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifycoloumnsIdeasTable extends Migration
{


   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('ideas', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    
        
         Schema::table('ideas', function (Blueprint $table) {
            $table->text('description')->change();
            $table->string('segments',100)->change();
            $table->string('price',20)->change();
            $table->string('success_probability',20)->change();
            $table->string('approximate_budget',20)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('ideas');
    }
}
