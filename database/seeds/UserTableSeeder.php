<?php
use Illuminate\Database\Seeder;
use App\User as User;
  
class UserTableSeeder extends Seeder {
  
    public function run() {
        User::truncate();
  
        User::create( [
            'email' => 'sheepy85@test.com' ,
            'password' => Hash::make( 'password' ) ,
            'firstname' => 'sheepy' ,
            'lastname' => '85' ,
            'dob' => '27-05-2015' ,
            'phone' => '919999999999' ,
            'website' => 'http://sheepy85.dev' ,
        ] );
    }
}