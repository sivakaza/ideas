@extends('app')

@section('htmlheader_title')
    Register
@endsection

@section('content')


    
        

       

<!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    Dashboard
                    <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Profile</li>
                        <li class="active">{{{ $user->firstname  }}}</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
            <div class="col-md-4">
              <!-- Widget: user widget style 1 -->
              <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
                  <div class="widget-user-image">
                    <img class="img-circle" src="{{{ URL::to($user->avatar)  }}}" alt="User Avatar">
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username">{{{ $user->firstname  }}} {{{ $user->lastname  }}}</h3>
                  <h5 class="widget-user-desc">Lead Developer</h5>
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#">Projects <span class="pull-right badge bg-blue">31</span></a></li>
                    <li><a href="#">Tasks <span class="pull-right badge bg-aqua">5</span></a></li>
                    <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
                    <!-- <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li> -->
                  </ul>
                </div>
              </div><!-- /.widget-user -->
            </div>
            <div class="col-md-8">

       <div class="box box-widget">
                <div class="box-header with-border">
                  <div class="user-block">
                    <img class="img-circle" src="{{{ URL::to($user->avatar)  }}}" alt="user image">
                    <span class="username"><a href="#">{{{ $user->firstname  }}}</a></span>
                    <span class="description">Shared publicly - 7:30 PM Today</span>
                  </div><!-- /.user-block -->
                  <div class="box-tools">
                   
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                
                 @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                 @if(Session::has('message'))
                          <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                 @endif
                <?php Session::forget('message'); ?>

                <form action="{{ URL::to('profile/') }}/{{ $user->id }}" method="post" name="editprofile" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="hidden" name="avtar_previous" value="{{ $user->avatar }}">
                
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="First Name" name="firstname" value="@if(!old('firstname')){{{ $user->firstname }}}@else{{ old('firstname') }}@endif"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Last Name" name="lastname" value="@if(!old('lastname')){{{ $user->lastname }}}@else{{ old('lastname') }}@endif"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" placeholder="Email" name="email" value="@if(!old('email')){{{ $user->email }}}@else{{ old('email') }}@endif" readonly />
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="phone"  class="form-control" value="@if(!old('phone')){{{ $user->phone }}}@else{{ old('phone') }}@endif" placeholder="Contact number">
                    <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                </div>
               
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="dob" placeholder="Date of Birth" value="@if(!old('dob')){{{ $user->dob }}}@else{{ old('dob') }}@endif" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        
                <div class="form-group has-feedback">
                    <input type="file" class="form-control" name="avtar" value="" />
                    <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                </div>
               
              <div class="form-group">
               <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
              </div>
            </form>

                 
              </div>
                      </div>
            <!-- <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>
            </div> -->

            
        </div><!-- /.form-box -->
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

        
   

        
    


@endsection
