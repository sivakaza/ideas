@extends('layouts.frontdefault')
@section('content')    
<div class="inner-banner"><div class="page-title"><h1 class="wow fadeInDown">Field reports</h1></div></div>


<section class="field-reports mar-t-30">
<div class="container">

<div class="row mar-t-30 mar-b-40">
<div class="cartegory wow fadeInDown">

    <div class="col-md-3">
    <label>Cartegory</label>
    <select id="example-getting-started" multiple="multiple" class="multiselect">
        <option value="Automoti">Automoti</option>
        <option value="Electronic">Electronic</option>
        <option value="Computer">Computer</option>
        <option value="Constructio">Constructio</option>
        <option value="Educati">Educati</option>
        <option value="Entertainm">Entertainm</option>
    </select>
    </div>
    
     <div class="col-md-3">
     <label>Industry/sector</label>
    <select id="example-getting-started2" multiple="multiple" class="multiselect">
        <option value="Automoti">Automoti</option>
        <option value="Electronic">Electronic</option>
        <option value="Computer">Computer</option>
        <option value="Constructio">Constructio</option>
        <option value="Educati">Educati</option>
        <option value="Entertainm">Entertainm</option>
    </select>
    </div>
    
    <div class="col-md-3">
     <label>Location</label>
    <select id="example-getting-started3" multiple="multiple" class="multiselect">
        <option value="Hyderabad">Hyderabad</option>
        <option value="bangalore">bangalore</option>
        <option value="chennai">chennai</option>
        <option value="Mumbai">Mumbai</option>
        <option value="Kalkata">Kalkata</option>
        <option value="Delhi">Delhi</option>
    </select>
    </div>
    
    
    <div class="col-md-2">
     <label>Search</label>
     <input type="text" placeholder="Search" id="" class="form-control inputsty">
    </div>
    
    <div class="col-md-1">
       <button class="btn btn-sty mar-t-35" type="submit">Submit</button>
    </div>     

    

<span class="clearfix"></span>
</div>
</div>


<div class="row">


   <div class="col-md-4 wow fadeInDown">
    <div class="custom-thumbnail round">
      <div class="head-img"> 
         <div  class="img-circle"><img src="img/emp1.jpg" alt=""></div>
         <div class="em-name"><a href="fieldreportin.html">Henry Richards</a></div>
      </div>
      <div class="content">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="57%"><span data-js-prompt="&amp;#xe005;" data-icon="" aria-hidden="true" class="glyph-item mega"></span>Employee ID</td>
            <td width="43%"><span class="emid">11223345</span></td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe075;"></span>Date & Time</td>
            <td>31-Apr-2015</td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe056;"></span>Industry/sector</td>
            <td>R&D </td>
          </tr>
          <tr class="boder-b-n">
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe096;"></span>Current Location</td>
            <td>Bangalore</td>
          </tr>
        </table>
      </div>
    </div>
   </div>
   

   <div class="col-md-4 wow fadeInDown">
    <div class="custom-thumbnail round">
      <div class="head-img"> 
         <div  class="img-circle"><img src="img/emp1.jpg" alt=""></div>
         <div class="em-name"><a href="fieldreportin.html">Henry Richards</a></div>
      </div>
      <div class="content">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="57%"><span data-js-prompt="&amp;#xe005;" data-icon="" aria-hidden="true" class="glyph-item mega"></span>Employee ID</td>
            <td width="43%"><span class="emid">11223345</span></td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe075;"></span>Date & Time</td>
            <td>31-Apr-2015</td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe056;"></span>Industry/sector</td>
            <td>R&D </td>
          </tr>
          <tr class="boder-b-n">
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe096;"></span>Current Location</td>
            <td>Bangalore</td>
          </tr>
        </table>
      </div>
    </div>
   </div>
   
   
   <div class="col-md-4 wow fadeInDown">
    <div class="custom-thumbnail round">
      <div class="head-img"> 
         <div  class="img-circle"><img src="img/emp1.jpg" alt=""></div>
         <div class="em-name"><a href="fieldreportin.html">Henry Richards</a></div>
      </div>
      <div class="content">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="57%"><span data-js-prompt="&amp;#xe005;" data-icon="" aria-hidden="true" class="glyph-item mega"></span>Employee ID</td>
            <td width="43%"><span class="emid">11223345</span></td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe075;"></span>Date & Time</td>
            <td>31-Apr-2015</td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe056;"></span>Industry/sector</td>
            <td>R&D </td>
          </tr>
          <tr class="boder-b-n">
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe096;"></span>Current Location</td>
            <td>Bangalore</td>
          </tr>
        </table>
      </div>
    </div>
   </div>
   
   
   <div class="col-md-4 wow fadeInDown">
    <div class="custom-thumbnail round">
      <div class="head-img"> 
         <div  class="img-circle"><img src="img/emp1.jpg" alt=""></div>
         <div class="em-name"><a href="fieldreportin.html">Henry Richards</a></div>
      </div>
      <div class="content">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="57%"><span data-js-prompt="&amp;#xe005;" data-icon="" aria-hidden="true" class="glyph-item mega"></span>Employee ID</td>
            <td width="43%"><span class="emid">11223345</span></td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe075;"></span>Date & Time</td>
            <td>31-Apr-2015</td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe056;"></span>Industry/sector</td>
            <td>R&D </td>
          </tr>
          <tr class="boder-b-n">
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe096;"></span>Current Location</td>
            <td>Bangalore</td>
          </tr>
        </table>
      </div>
    </div>
   </div>
   
   
   <div class="col-md-4 wow fadeInDown">
    <div class="custom-thumbnail round">
      <div class="head-img"> 
         <div  class="img-circle"><img src="img/emp1.jpg" alt=""></div>
         <div class="em-name"><a href="fieldreportin.html">Henry Richards</a></div>
      </div>
      <div class="content">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="57%"><span data-js-prompt="&amp;#xe005;" data-icon="" aria-hidden="true" class="glyph-item mega"></span>Employee ID</td>
            <td width="43%"><span class="emid">11223345</span></td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe075;"></span>Date & Time</td>
            <td>31-Apr-2015</td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe056;"></span>Industry/sector</td>
            <td>R&D </td>
          </tr>
          <tr class="boder-b-n">
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe096;"></span>Current Location</td>
            <td>Bangalore</td>
          </tr>
        </table>
      </div>
    </div>
   </div>
   
   
   <div class="col-md-4 wow fadeInDown">
    <div class="custom-thumbnail round">
      <div class="head-img"> 
         <div  class="img-circle"><img src="img/emp1.jpg" alt=""></div>
         <div class="em-name"><a href="fieldreportin.html">Henry Richards</a></div>
      </div>
      <div class="content">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="57%"><span data-js-prompt="&amp;#xe005;" data-icon="" aria-hidden="true" class="glyph-item mega"></span>Employee ID</td>
            <td width="43%"><span class="emid">11223345</span></td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe075;"></span>Date & Time</td>
            <td>31-Apr-2015</td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe056;"></span>Industry/sector</td>
            <td>R&D </td>
          </tr>
          <tr class="boder-b-n">
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe096;"></span>Current Location</td>
            <td>Bangalore</td>
          </tr>
        </table>
      </div>
    </div>
   </div>
   
   
   <div class="col-md-4 wow fadeInDown">
    <div class="custom-thumbnail round">
      <div class="head-img"> 
         <div  class="img-circle"><img src="img/emp1.jpg" alt=""></div>
         <div class="em-name"><a href="fieldreportin.html">Henry Richards</a></div>
      </div>
      <div class="content">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="57%"><span data-js-prompt="&amp;#xe005;" data-icon="" aria-hidden="true" class="glyph-item mega"></span>Employee ID</td>
            <td width="43%"><span class="emid">11223345</span></td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe075;"></span>Date & Time</td>
            <td>31-Apr-2015</td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe056;"></span>Industry/sector</td>
            <td>R&D </td>
          </tr>
          <tr class="boder-b-n">
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe096;"></span>Current Location</td>
            <td>Bangalore</td>
          </tr>
        </table>
      </div>
    </div>
   </div>
   

   <div class="col-md-4 wow fadeInDown">
    <div class="custom-thumbnail round">
      <div class="head-img"> 
         <div  class="img-circle"><img src="img/emp1.jpg" alt=""></div>
         <div class="em-name"><a href="fieldreportin.html">Henry Richards</a></div>
      </div>
      <div class="content">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="57%"><span data-js-prompt="&amp;#xe005;" data-icon="" aria-hidden="true" class="glyph-item mega"></span>Employee ID</td>
            <td width="43%"><span class="emid">11223345</span></td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe075;"></span>Date & Time</td>
            <td>31-Apr-2015</td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe056;"></span>Industry/sector</td>
            <td>R&D </td>
          </tr>
          <tr class="boder-b-n">
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe096;"></span>Current Location</td>
            <td>Bangalore</td>
          </tr>
        </table>
      </div>
    </div>
   </div>
   
   
   <div class="col-md-4 wow fadeInDown">
    <div class="custom-thumbnail round">
      <div class="head-img"> 
         <div  class="img-circle"><img src="img/emp1.jpg" alt=""></div>
         <div class="em-name"><a href="fieldreportin.html">Henry Richards</a></div>
      </div>
      <div class="content">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="57%"><span data-js-prompt="&amp;#xe005;" data-icon="" aria-hidden="true" class="glyph-item mega"></span>Employee ID</td>
            <td width="43%"><span class="emid">11223345</span></td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe075;"></span>Date & Time</td>
            <td>31-Apr-2015</td>
          </tr>
          <tr>
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe056;"></span>Industry/sector</td>
            <td>R&D </td>
          </tr>
          <tr class="boder-b-n">
            <td><span class="glyph-item mega" aria-hidden="true" data-icon="" data-js-prompt="&amp;#xe096;"></span>Current Location</td>
            <td>Bangalore</td>
          </tr>
        </table>
      </div>
    </div>
   </div>
   
</div>


</div>
</section>
@stop