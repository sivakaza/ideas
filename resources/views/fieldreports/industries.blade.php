<!-- resources/views/user.blade.php -->
@extends('app')

@section('htmlheader_title')
    Register
@endsection

@section('content')


    
        

       

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Dashboard
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Industries</li>
        
    </ol>
</section>
                <!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-10">

       <div class="box box-widget">
                <div class="box-header with-border">
                <div class="user-block">
                    <span><i class="glyphicon glyphicon-users"></i></span>
                    <span class="username">Industry</span>
                    <span class="description">Manage Industries</span>
                  </div><!-- /.user-block -->
                  <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <!-- Data Table - List USERS -->
                  <table class="table table-bordered" id="industry-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                                <th>Created Date</th>
                            </tr>
                        </thead>
                    </table>
                </div>
        </div>
</div>
<div class="col-md-2">
 <div class="box box-widget">
    <button class="btn btn-lg  btn-success" id="addIndustry" style="margin: 5px 0;"><i class="glyphicon glyphicon-plus"></i> Add Industry</button>
    <button class="btn btn-lg  btn-primary" id="editIndustry" style="margin: 5px 0;"><i class="glyphicon glyphicon-edit"></i> Edit Industry</button>
    <button class="btn btn-lg  btn-danger" id="deleteIndustry" style="margin: 5px 0;"><i class="glyphicon glyphicon-trash"></i> Delete Industry</button>
    
 </div>
</div>
    
    
</div>
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

        
   

        
    


@endsection


