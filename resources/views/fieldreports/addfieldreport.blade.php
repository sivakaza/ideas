@extends('app')

@section('htmlheader_title')
    Register
@endsection

@section('content')


    
        

       

<!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    Dashboard
                    <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Market Report</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
            
            <div class="col-md-12">

       <div class="box box-widget">
                <div class="box-header with-border">
                  <div class="user-block">
                    
                    <span> <h3> <i class="fa fa-briefcase fa-lg"></i> Add New Report</h3> </span>
                    
                  </div><!-- /.user-block -->
                  <div class="box-tools">
                   
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                
                 @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                 @if(Session::has('message'))
                          <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                 @endif
                <?php Session::forget('message'); ?>
              <form name="businessidea" method="post" enctype="multipart/form-data" action="{{ url('add_idea') }}">
              <div class="col-md-5">
                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group has-feedback">
                    <select name="category" class="form-control">
                       <option value="">-- Select Category --</option>
                       <option value="Entertainment" >Entertainment</option>
                       
                    </select>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Name" name="title" value="{{ old('title') }}"/>
                    <span class="form-control-feedback"></span>
                </div>
              
                <div class="form-group">
                  <div class='input-group date' id='datetimepicker1'>
                      <input type='text' class="form-control" name="dob" placeholder="Submission End Date" value="end_date" />
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
              </div>
               
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Price" name="price" value="{{ old('price') }}"/>
                    <span class="glyphicon glyphicon-tags form-control-feedback"></span>
                </div>
               

                 <!-- checkbox -->
                  <div class="form-group">
                      <h4>Segment Options  </h4>
                     <label>
                             <input type="checkbox" class="flat-red" value="Direct Pay" name="segment"> Direct Pay
                     </label> 
                     <label>
                             <input type="checkbox" class="flat-red" value="Hire a Consultant" name="segment"> Hire a Consultant
                     </label>    
                  </div>

               
                <div class="form-group has-feedback">
                    <h4>Select File(s)</h4>
                    <input type="file" class="form-control" multiple="" name="idea_files[]" value="" />
                    <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                </div>
               

               <div class="form-group">
                      <h4>Status  </h4>
                     <label>
                             <input type="radio" class="flat-red" value="Active" name="status"> Active
                     </label> 
                     <label>
                             <input type="radio" class="flat-red" value="Inactive" name="status"> Inactive
                     </label>    
                  </div>

                
                

                 </div>           
                  <div class="col-md-7">
                   <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Location" name="location" value="{{ old('location') }}"/>
                    <span class="glyphicon glyphicon-tags form-control-feedback"></span>
                </div>
                 <div class="form-group has-feedback">
                    <textarea class="" id="editor1" name="short_description" placeholder="Short Description" style="width: 100%; height: 50px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    <span class="form-control-feedback"></span>
                       <!--</div> /. tools --> 
                 </div>
                 <div class="form-group has-feedback">
                    <textarea class="textarea" id="editor1" name="description" placeholder="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    <span class="form-control-feedback"></span>
                       <!--</div> /. tools --> 
                 </div>

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4 pull-right">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                    </div><!-- /.col -->
                </div>

              </div>
            </form>
                 
              </div>
                      </div>
           
              
        </div><!-- /.form-box -->
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

        <script src="../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script>
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
      });
    </script>

        
    


@endsection