@extends('app')

@section('htmlheader_title')
    Register
@endsection

@section('content')


    
        

       

<!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    Dashboard
                    <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><a href="{{{ URL::to('ideas') }}}">Business Ideas</a></li>
                        <li class="active">Edit Idea</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
            
            <div class="col-md-12">

       <div class="box box-widget">
                <div class="box-header with-border">
                  <div class="user-block">
                    
                    <span> <h3> <i class="fa fa-briefcase fa-lg"></i>Edit Idea</h3> </span>
                    
                  </div><!-- /.user-block -->
                  <div class="box-tools">
                   
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                
                 @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                 @if(Session::has('message'))
                          <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                 @endif
                <?php Session::forget('message'); ?>
                



                {!! Form::open(array('url' => 'update_idea/'.$idea->id, 'files' => true)) !!}

              <div class="col-md-5">
                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $idea->id }}" />
                <div class="form-group has-feedback">
                
                {!! Form::select('sector', ['' => 'Select Sector'] +$sectors->toArray(), $idea->category ? $idea->category : old('sector'), ['class' => 'form-control','required']) !!}  
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Name" name="title" value="{{ (!old('title')) ? $idea->title : old('title') }}"/>
                    <span class="fa fa-info form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Price" name="price" value="{{ (!old('price')) ? $idea->price : old('price') }}"/>
                    <span class="fa fa-money form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Success Proability" name="success_probability" value="{{ (!old('success_probability')) ? $idea->success_probability : old('success_probability') }}"/>
                    <span class="fa  fa-thumbs-o-up form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Approximate Budget" name="approximate_budget" value="{{ (!old('approximate_budget')) ? $idea->approximate_budget : old('approximate_budget') }}"/>
                    <span class="fa fa-money form-control-feedback"></span>
                </div>
                
                  <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Location" name="location" value="{{ (!old('location'))? $idea->location: old('location') }}"/>
                    <span class="fa fa-map-marker form-control-feedback"></span>
                </div>
                    

                 <!-- checkbox -->
                  <div class="form-group">
                      <h4>Segment Options  </h4>
                     <label>
                             {!! Form::checkbox('segments[]', 'Direct Pay',  in_array('Direct Pay', unserialize($idea->segments)) ? true : null , ['class'=>'flat-red'] ) !!}Direct Pay
                     </label> 
                     <label>
                             {!! Form::checkbox('segments[]', 'Hire a Consultant',  in_array('Hire a Consultant', unserialize($idea->segments)) ? true : null, ['class'=>'flat-red'] ) !!}Hire a Consultant
                     </label>    
                  </div>

             
             

                 
             

               <div class="form-group">
                      <label>Status</label>
                     <label>
                             {!! Form::radio('status', 'Active', $idea->status == 'Active' ? true : null, ['class'=>'flat-red']) !!} Active
                     </label> 
                     <label>
                             {!! Form::radio('status', 'Inctive', $idea->status == 'Inactive' ? true : null, ['class'=>'flat-red']) !!} Inactive
                     </label>    
                  </div>

                
                

                 </div>           
                  <div class="col-md-7">
                                    <div class="form-group has-feedback">
                    <textarea class="" id="editor1" name="short_description" placeholder="Short Description" style="width: 100%; height: 50px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ (!old('short_description')) ? $idea->short_description : old('short_description') }}</textarea>
                    <span class="form-control-feedback"></span>
                       <!--</div> /. tools --> 
                 </div>
                 <div class="form-group has-feedback">
                    <textarea class="textarea" id="editor1" name="description" placeholder="Description" style="width: 100%; height: 180px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ (!old('description')) ? $idea->short_description : old('description') }}</textarea>
                    <span class="form-control-feedback"></span>
                       <!--</div> /. tools --> 
                 </div>
                 
                 <div class="form-group has-feedback">
                    <label>Select Image</label>
                    {!! Form::file('image', ['class' => 'form-control']); !!}
                    <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                </div>
               <div class="form-group">
                    
                    <label for="">Previous Image</label>
                    <a href="{{ URL::to($idea->image) }}" target="_blank">{{ $idea->image }}</a>
                    <input type="hidden" name="previous_image" value="{{ $idea->image }}" />


                  </div>

                  <div class="form-group has-feedback">
                    <label>Select File(s)</label>
                    <input type="file" class="form-control" multiple="" name="idea_files[]" value="" />
                    <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                </div>

               <div class="form-group">
                    
                    
                    <label for="">Previous Files</label>
                    @foreach($files as $file)
                       <br />
                       <input type="hidden" name="previous_files[]" value="{{ $file->path }}">
                         {!! link_to("businessidea/download/".$file->image,$file->image) !!}
                    @endforeach


                  </div>

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4 pull-right">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                    </div><!-- /.col -->
                </div>

              </div>
           {!! Form::close() !!}
                 
              </div>
                      </div>
           
              
        </div><!-- /.form-box -->
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

        
        
    


@endsection