@extends('layouts.frontdefault')
@section('content')    
<div class="inner-banner"><div class="page-title"><h1 class="wow fadeInDown">BUsiness Ideas</h1></div></div>
<div class="container">
    @foreach ($ideas as $idea)
        {{ $idea->title }}
    @endforeach
</div>

{!! $ideas->render() !!}

@end