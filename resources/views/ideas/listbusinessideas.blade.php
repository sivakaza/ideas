<!-- resources/views/user.blade.php -->
@extends('app')

@section('htmlheader_title')
    Register
@endsection

@section('content')


    
        

       

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Dashboard
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Business Ideas</li>
        
    </ol>
</section>
                <!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">

       <div class="box box-widget">
                <div class="box-header with-border">
                <div class="user-block">
                    <span><i class="glyphicon glyphicon-briefcase"></i></span>
                    <span class="username">Manage Business Ideas</span>
                    
                  </div><!-- /.user-block -->
                  <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <!-- Data Table - List USERS -->

                <button class="btn btn-lg  btn-success" id="addIdea" style="margin: 5px 0;"><i class="glyphicon glyphicon-plus"></i> Add Idea</button>
                <button class="btn btn-lg  btn-primary" id="editIdea" style="margin: 5px 0;"><i class="glyphicon glyphicon-edit"></i> Edit Idea</button>
                <button class="btn btn-lg  btn-danger" id="deleteIdea" style="margin: 5px 0;"><i class="glyphicon glyphicon-trash"></i> Delete Idea</button>
                <button class="btn btn-lg  btn-warning" id="MailList" style="margin: 5px 0;"><i class="glyphicon glyphicon-envelope"></i> Mail Listings</button>
                <button class="btn btn-lg  btn-primary" id="ViewFiles" style="margin: 5px 0;"><i class="glyphicon glyphicon-file"></i> View Files</button>
                  <table class="table table-bordered" id="idea-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Sector</th>
                                <th>Title</th>
                                <th>Short Desc</th>
                                <th>Price</th>
                                <th>Approximate Budget</th>
                                <th>Success Probability</th>
                                <th>Location</th>
                                <th>Created By</th>
                                <th>Updated At</th>
                            </tr>
                        </thead>
                    </table>
                </div>
        </div>
</div>

    
    
</div>
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

        
   

        
    


@endsection


