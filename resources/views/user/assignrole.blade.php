@extends('app')

@section('htmlheader_title')
    Register
@endsection

@section('content')


    
 
       

<!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    Dashboard
                    <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><a href="{{{ URL::to('users') }}}">Users</a></li>
                        <li class="active">Assign Role</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
            
            <div class="col-md-8">

       <div class="box box-widget">
                <div class="box-header with-border">
                  <div class="user-block">
                    
                    <span> <h3> <i class="fa fa-user fa-lg"></i> Assign User Roles</h3> </span>
                    
                  </div><!-- /.user-block -->
                  <div class="box-tools">
                   
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                
                 @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                 @if(Session::has('message'))
                          <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                 @endif
                <?php Session::forget('message'); ?>
                
                <form action="{{ url('assign_role') }}/{{ $user->id }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $user->id }}" />
                
                <!-- checkbox -->
                  <div class="form-group">
                  @foreach($roles as $role)
                         <div class="checkbox">
                            <label>
                              @if(in_array($role->id, $existingRoles))
                              <input type="checkbox" class="flat-red" value="{{  $role->id }}" name="role[]" checked>
                              @else
                               <input type="checkbox" class="flat-red" value="{{  $role->id }}" name="role[]"> 
                              @endif
                               {{  $role->name }}
                           </label> 
                         </div> 

                  @endforeach
                    
                   <!--  <label>
                      <input type="checkbox" class="flat-red">
                    </label>
                    <label>
                      <input type="checkbox" class="flat-red" disabled>
                      Flat green skin checkbox
                    </label> -->
                  </div>

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4 pull-right">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Assign Role(s)</button>
                    </div><!-- /.col -->
                </div>
            </form>

                 
              </div>
                      </div>
            <!-- <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>
            </div> -->

            
        </div><!-- /.form-box -->
        <div class="col-md-4">
              <!-- Widget: user widget style 1 -->
              <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
                  <div class="widget-user-image">
                    <img class="img-circle" src="{{{ URL::to($user->avatar)  }}}" alt="User Avatar">
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username">{{{ $user->firstname  }}} {{{ $user->lastname  }}}</h3>
                  <h5 class="widget-user-desc">@foreach($existingRoles as $existingRole){{ $existingRole['name'] }} @endforeach</h5>
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#">Projects <span class="pull-right badge bg-blue">31</span></a></li>
                    <li><a href="#">Tasks <span class="pull-right badge bg-aqua">5</span></a></li>
                    <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
                    <!-- <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li> -->
                  </ul>
                </div>
              </div><!-- /.widget-user -->
            </div>
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

        
   

        
    


@endsection
