@extends('app')

@section('htmlheader_title')
    Register
@endsection

@section('content')


    
 
       

<!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    Dashboard
                    <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><a href="{{{ URL::to('users') }}}">Users</a></li>
                        <li class="active">Add User</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
            
            <div class="col-md-8">

       <div class="box box-widget">
                <div class="box-header with-border">
                  <div class="user-block">
                    
                    <span> <h3> <i class="fa fa-user fa-lg"></i> Add New User</h3> </span>
                    
                  </div><!-- /.user-block -->
                  <div class="box-tools">
                   
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                
                 @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                 @if(Session::has('message'))
                          <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                 @endif
                <?php Session::forget('message'); ?>
                
                <form action="{{ url('add_user') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <div class="form-group has-feedback">
                    <select name="role" class="form-control">
                       <option value="">-- Select User Role --</option>
                        @foreach($roles as $role)
                         <option value="{{  $role->id }}" >{{  $role->name }}</option>                        
                        @endforeach
                    </select>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="First Name" name="firstname" value="{{ old('firstname') }}"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Last Name" name="lastname" value="{{ old('lastname') }}"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation"/>
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="phone"  class="form-control" value="{{ old('phone') }}" placeholder="Contact number">
                    <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                </div>
               
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="dob" placeholder="Date of Birth" value="" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        
                <div class="form-group has-feedback">
                    <input type="file" class="form-control" name="avtar" value="" />
                    <span class="glyphicon glyphicon-picture form-control-feedback"></span>
                </div>
               

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4 pull-right">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                    </div><!-- /.col -->
                </div>
            </form>

                 
              </div>
                      </div>
            <!-- <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>
            </div> -->

            
        </div><!-- /.form-box -->
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

        
   

        
    


@endsection
