<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>@section('title')
     Rajan Business Ideas
      @show
</title>
<!-- Bootstrap core CSS -->
<!-- <link href="css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet"> -->
<!-- Custom Styles -->
<!-- <link href="css/custom-styles.css" rel="stylesheet">
<link href="css/content_slider_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/animate.css" media="screen"> -->

{!! HTML::style('css/bootstrap.css') !!}
{!! HTML::style('css/font-awesome.css') !!}
{!! HTML::style('css/custom-styles.css') !!}
{!! HTML::style('css/content_slider_style.css') !!}
{!! HTML::style('css/animate.css') !!}
{!! HTML::style('css/simple-line-icons.css') !!}
{!! HTML::style('css/prettify.css') !!}
{!! HTML::style('css/bootstrap-multiselect.css') !!}

{!! HTML::script('js/html5shiv.js') !!}
{!! HTML::script('js/respond.min.js') !!}

<!-- <script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script> -->
</head>
<body>


                @include('partials.frontheader')
                <!-- Content -->
                @yield('content')
                <!-- ./ content -->
                @include('partials.frontfooter')
                @include('partials.frontScripts')




    
</body>
</html>
