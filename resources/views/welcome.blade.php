@extends('layouts.frontdefault')
@section('content')             

<div id="banner"><img src="img/banner.jpg" alt=""></div>

<section id="about-row">
    <div class="container">
    <div class="row about-box wow fadeInDown">
    
      <div class="col-md-4 col-lg-4 Rborder pad-b-60 pad-t-60">
      <i class="fa fa-coffee"></i>
        <h2>Business Planning</h2>
        <div class="content">
            <a href="#">People looking for a good business plan to launch their own business can browse through our list of Business ideas, and get the business idea that best fits their requirement and shows highest compatibility with their working pattern.</a>
        </div>
      </div>
      
      <div class="col-md-4 col-lg-4 Rborder pad-b-60 pad-t-60">
      <i class="fa fa-lightbulb-o"></i>
        <h2>Evaluate your Business Idea</h2>
        <div class="content">
            <a href="#">If you have a business idea and are not sure if it is going to be a successful one or not, our team will evaluate your business idea and chip in our expertise to help you with suggestions or improvements about your business idea.</a>
        </div>
      </div>
      
      <div class="col-md-4 col-lg-4 pad-b-60 pad-t-60">
      <i class="fa fa-sitemap"></i>
        <h2>How to Start a Business</h2>
        <div class="content">
            <a href="#">For few selected Business Ideas, our tem will help setup the business and start the execution part of the Business plan for a minimal monetary fee+ 10% stake in the company, the client only has to invest in developing his business.</a>
        </div>
      </div>
      
    </div>
    </div>
</section>




<section class="module parallax parallax-1">
    <div class="container">
            
                <div class="services wow fadeInDown" data-animate="bounceIn">
                    <div class="title"><h1>Why <span>rajan</span> business ideas</h1></div>
                    <div class="content">
                        <p>Experience, trusted brand, expert team makes Rajan Business Ideas the preferred brand in the Entrepreneurs world. Our objective is to help the entrepreneurs at all steps of their endeavor to become a successful business man with a well-established business vertical.</p>
    <button class="btn btn-primary btn-lg" ><a href="">Read More</a></button>
                    </div>
                    
    </div>
    
    
       <div class="container">
      <div id="sti-menu" class="sti-menu mar-t-50 wow fadeInDown">
            
        <div data-hovercolor="#09d0b1" class="col-md-4">
          <a href="#">
                      <span data-type="icon" class="sti-icon sti-icon-care sti-item"></span>
            <h2 data-type="mText" class="sti-item">Reliable</h2>
                        <h3 data-type="sText" class="sti-item">We are the most trusted brand in the industry with an expert team whose only focus is on our clients Business success as the top priority.</h3>
          </a>
        </div>
                
                
                <div data-hovercolor="#09d0b1" class="col-md-4">
          <a href="#">
            <span data-type="icon" class="sti-icon sti-icon-amplifying sti-item"></span>
                        <h2 data-type="mText" class="sti-item">Knowledgeable</h2>
                        <h3 data-type="sText" class="sti-item">We possess the perfect knowledge base to build a successful Business Strategy for a successful Business plan. <!--Our cross domain knowledge help in predictive modeling which will help in countering any farfetched obstacles that the business might encounter in the near future.--></h3>
          </a>
                </div>
                
                
                <div data-hovercolor="#09d0b1" class="col-md-4">
          <a href="#">
                      <span data-type="icon" class="sti-icon sti-icon-trusted sti-item"></span>
            <h2 data-type="mText" class="sti-item">Successful</h2>
                        <h3 data-type="sText" class="sti-item">We are the first company to have successfully developed a platform for to-be entrepreneurs and deliver satisfying services at the best quality.</h3>
          </a>
        </div>
        
        
        
        
      </div>
      
    </div>         
    
    </div>
</section>
      
      
<section class="our-services wow fadeInDown">
    <div class="container">
    <div class="row pad-t-50">
    
        <div class="col-md-4 box">
        <div class="thumbnail">
          <img alt="..." class="img-responsive" src="img/img1.png">
          <div class="caption">
             
              <h3>Professional Services</h3>
            <p>Includes Business Plan evaluation, Business execution strategy development and support in Business expansion opportunity realization.</p>
            <a role="button" class="btn btn-default" href="#">Readmore</a>
            
          </div>
        </div>
      </div>
      
      
      
      <div class="col-md-4 box">
        <div class="thumbnail">
          <img alt="..." class="img-responsive" src="img/img2.png">
          <div class="caption">
              <h3>Field Reports</h3>
            <p>You can purchase the field reports submitted by our on field executives explaining the field scenario of different sectors from the grassroots level. </p>
            <a role="button" class="btn btn-default" href="#">Readmore</a>
          </div>
        </div>
      </div>
      
      
      <div class="col-md-4 box">
        <div class="thumbnail">
          <img alt="..." class="img-responsive" src="img/img3.png">
          <div class="caption">
             
              <h3>premium and free</h3>
            <p>the business ideas are broadly classified into premium and ordinary, wherein the premium ideas for people incurring very high investments into a very high risk segments.</p>
            <a role="button" class="btn btn-default" href="#">Readmore</a>
    
          </div>
        </div>
      </div>
    
    
    
    </div>
    </div>
</section>



<section class="customers wow fadeInDown">
<div class="container mar-t-90">
  <div class="row">
    <div class="title"><h1>happy customers</h1>
    </div>

    <div class="carousel-row bounceout">
        <a href="#" class="prev">&lsaquo;</a>
        <div class="carousel" style="visibility:hidden;">
            <ul>
                <li><img src="img/1.jpg"></li>
                <li><img src="img/2.jpg"></li>
                <li><img src="img/3.jpg"></li>
                <li><img src="img/4.jpg"></li>
                <li><img src="img/5.jpg"></li>
                <li><img src="img/6.jpg"></li>
                <li><img src="img/7.jpg"></li>
                <li><img src="img/8.jpg"></li>
                <li><img src="img/9.jpg"></li>
                <li><img src="img/10.jpg"></li>
                <li><img src="img/11.jpg"></li>
            </ul>
        </div>
        <a href="#" class="next">&rsaquo;</a>
    </div>
    
    
  </div>
</div>
</section>


<section class="testimonials wow fadeInDown">
    <div class="container mar-t-90">
        <div class="row">
            <div class="title"><h1>Testimonials</h1>
          <h5>They are happy to work with us</h5></div>
        
             <div style="max-width: 915px;" class="main_content_slider_wrapper">
  <div style="width: 915px; height: 478px;" class="content_slider_wrapper" id="slider1">
    
    
    
    <div style="left: 2px; width: 915px; height: 242px; transform: scale(1, 1);" class="circle_slider">

      <div class="circle_slider_thumb all_around_circle_3" style="left: 811px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/6.jpg" class="all_around_img_3">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_4" style="left: 915px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 84px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 84px; height: 84px; border-radius: 84px; display: inline-block;" src="img/testimonials/5.jpg" class="all_around_img_4">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_5" style="left: -104px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/6.jpg" class="all_around_img_5">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_6" style="left: 0px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/7.jpg" class="all_around_img_6">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_0" style="left: 104px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/1.jpg" class="all_around_img_0">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_1" style="left: 208px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/2.jpg" class="all_around_img_1">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_2" style="left: 332px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 0.5px; width: 231px; height: 231px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 231px; height: 231px; border-radius: 231px; display: inline-block;" src="img/testimonials/3.jpg" class="all_around_img_2">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_3" style="left: 603px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/4.jpg" class="all_around_img_3">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_4" style="left: 707px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/5.jpg" class="all_around_img_4">
      
      </div>
      
      <div style="left: 304px; top: 90px;" class="circle_slider_nav_left circle_slider_no_border"> 
      
      <img style="z-index: 1000; margin-top: 15px; left: 0px;" src="img/left2.png" alt="left"></div>
      
      <div style="left: 563px; top: 90px;" class="circle_slider_nav_right circle_slider_no_border"> 
      
      <img style="z-index: 1000; margin-top: 15px; left: 4px;" src="img/right2.png" alt="right">
      
      </div>
    </div>
    
    
    <div class="circle_slider_text_wrapper" id="sw0" style="width: 893px; top: 0px; left: 0px; display: none;">
      <!-- content for the first layer, id="sw0" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Amanda McHilly</h3>
        <br>
        <span><span class="bold">Company: </span>Senior PHP Developer</span><br>
        <br>
        <span><span class="bold">About: </span>Amanda specializes in 
        establishing and managing internal infrastructure and process for 
        educational institutions, research centers, and non-profits. She is 
        experienced in project management, finance, organizational leadership, 
        and program implementation, all driven by a commitment to provide 
        outstanding service.</span><br>
      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw1" style="width: 893px; top: 0px; left: 0px; display: none;">
      <!-- content for the second layer, id="sw1" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Ben Gillenhall</h3>
        <br>
        <span><span class="bold">Company: </span>Company PR</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem.
         Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>

      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw2" style="width: 893px; top: 0px; left: 0px;">
      <!-- content for the third layer, id="sw2" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Giselle Smithsonian</h3>
        <br>
        <span><span class="bold">Company: </span>Company PR</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. 
        Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw3" style="display: none; width: 893px; top: 0px; left: 0px;">
      <!-- content for the fourth layer, id="sw3" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Rafael Munez</h3>
        <br>
        <span><span class="bold">Company: </span>Audio/Visual Effects</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. 
        Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw4" style="display: none; width: 893px; top: 0px; left: 0px;">
      <!-- content for the fifth layer, id="sw4" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Brenda Wolsh</h3>
        <br>
        <span><span class="bold">Company: </span>CEO of Legal Department</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. 
        Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw5" style="display: none; width: 893px; top: 0px; left: 0px;">
      <!-- content for the sixth layer, id="sw5" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Bradley Cooper</h3>
        <br>
        <span><span class="bold">Company: </span>Human Resources</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. 
        Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw6" style="display: none; width: 893px; top: 0px; left: 0px;">
      <!-- content for the seventh layer, id="sw6" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Gillian Aston-Martin</h3>
        <br>
        <span><span class="bold">Company: </span>HTML/CSS Coder</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem.
         Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
        
        </div>     
    </div>
</section>
@stop