@extends('layouts.frontdefault')
@section('content')             

<div class="inner-banner"><div class="page-title"><h1 class="wow fadeInDown">Services</h1></div></div>


<div class="container">
  <div class="row">
    <div class="about-page">
               <div class="content wow fadeInDown">
                 
                 <div class="box">
                 
                            <div class="title wow fadeInDown">
                 <h1>We are here for you</h1><h5>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</h5></div>

              <div class="col-md-5"><img src="img/img4.jpg" alt=""></div>
                    <div class="col-md-7 pad-t-30">Rajan Business Ideas was launched in September 2014, with a sole objective of helping the entrepreneurs to choose a successful business idea, or get their business idea evaluated and find the probability of success in the current market scenario. It has been observed that only 8% of the startups are able to sustain in India, and the remaining startups are shut down after a few months, there are several factors that lead to the failure of these startups. Our expert team at Rajan Business Ideas continuously monitors the startup scenario in India and continuously improve the Business Ideas that we offer depending on the current business market scenario. We continuously try and eliminate the factors which led to the failure of the earlier startups in similar verticals. We at Rajan Business Ideas reiterate every aspect of a business idea and try to improve the possibility of success of a business idea by putting the right strategy to execute the business. We also evaluate the business ideas and give a detailed report on the percentage possibility of success, and give suggestions to assure a definite success. 
                    </div>
                 </div>
                    
                    <div class="clearfix"></div>
                    
                    <div class="box mar-t-30">
                              <div class="title wow fadeInDown">
                 <h1>our vision</h1><h5>Sed ut perspiciatis unde omnis iste </h5></div>

                    
                    <div class="col-md-7 pad-t-60">Our Vision is to reach a point in the business world where we can perfect the science of entrepreneurship to such a level where the success rate is far above the failure rate for all Business plans provided by us.
                    </div>
                  <div class="col-md-5"><img src="img/img5.jpg" alt=""></div>
                </div>
                
                <div class="clearfix"></div>
                
                <div class="box mar-t-30">
                 <div class="title wow fadeInDown">
                 <h1>our mission</h1><h5>Sed ut perspiciatis unde omnis iste </h5></div>
                  <div class="col-md-5"><img src="img/img6.jpg" alt=""></div>
                    <div class="col-md-7 pad-t-60">Our mission is to educate all entrepreneurs about the current business market scenario and the risks involved in it, and guide them to setting up a successful business. And help them take an informed decision even before they fixate on a business idea. Because, as the idiom says “A good start is work half done”. Our mission is to assure that good start.
                    </div>
                </div>
                    
                    
                    
          </div>
        </div>
    </div>  
</div>

<section class="customers wow fadeInDown">
<div class="container mar-t-90">
  <div class="row">
    <div class="title"><h1>happy customers</h1></div>

    <div class="carousel-row bounceout">
        <a href="#" class="prev">&lsaquo;</a>
        <div class="carousel" style="visibility:hidden;">
            <ul>
                <li><img src="img/1.jpg"></li>
                <li><img src="img/2.jpg"></li>
                <li><img src="img/3.jpg"></li>
                <li><img src="img/4.jpg"></li>
                <li><img src="img/5.jpg"></li>
                <li><img src="img/6.jpg"></li>
                <li><img src="img/7.jpg"></li>
                <li><img src="img/8.jpg"></li>
                <li><img src="img/9.jpg"></li>
                <li><img src="img/10.jpg"></li>
                <li><img src="img/11.jpg"></li>
            </ul>
        </div>
        <a href="#" class="next">&rsaquo;</a>
    </div>
    
    
  </div>
</div>
</section>


<section class="testimonials wow fadeInDown">
    <div class="container mar-t-90">
        <div class="row">
            <div class="title"><h1>Testimonials</h1><h5>They are happy to work with us</h5></div>
        
             <div style="max-width: 915px;" class="main_content_slider_wrapper">
  <div style="width: 915px; height: 478px;" class="content_slider_wrapper" id="slider1">
    
    
    
    <div style="left: 2px; width: 915px; height: 242px; transform: scale(1, 1);" class="circle_slider">

      <div class="circle_slider_thumb all_around_circle_3" style="left: 811px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/6.jpg" class="all_around_img_3">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_4" style="left: 915px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 84px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 84px; height: 84px; border-radius: 84px; display: inline-block;" src="img/testimonials/5.jpg" class="all_around_img_4">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_5" style="left: -104px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/6.jpg" class="all_around_img_5">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_6" style="left: 0px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/7.jpg" class="all_around_img_6">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_0" style="left: 104px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/1.jpg" class="all_around_img_0">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_1" style="left: 208px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/2.jpg" class="all_around_img_1">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_2" style="left: 332px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 0.5px; width: 231px; height: 231px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      
      <img style="width: 231px; height: 231px; border-radius: 231px; display: inline-block;" src="img/testimonials/3.jpg" class="all_around_img_2">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_3" style="left: 603px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/4.jpg" class="all_around_img_3">
      
      </div>
      
      <div class="circle_slider_thumb all_around_circle_4" style="left: 707px; box-shadow: 0px 0px 0px rgb(255, 255, 255); top: 74px; width: 84px; height: 84px; border-radius: 231px; border: 0px solid rgb(40, 40, 40);"> 
      <img style="width: 84px; height: 84px; border-radius: 231px; display: inline-block;" src="img/testimonials/5.jpg" class="all_around_img_4">
      
      </div>
      
      <div style="left: 304px; top: 90px;" class="circle_slider_nav_left circle_slider_no_border"> 
      
      <img style="z-index: 1000; margin-top: 15px; left: 0px;" src="img/left2.png" alt="left"></div>
      
      <div style="left: 563px; top: 90px;" class="circle_slider_nav_right circle_slider_no_border"> 
      
      <img style="z-index: 1000; margin-top: 15px; left: 4px;" src="img/right2.png" alt="right">
      
      </div>
    </div>
    
    
    <div class="circle_slider_text_wrapper" id="sw0" style="width: 893px; top: 0px; left: 0px; display: none;">
      <!-- content for the first layer, id="sw0" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Amanda McHilly</h3>
        <br>
        <span><span class="bold">Company: </span>Senior PHP Developer</span><br>
        <br>
        <span><span class="bold">About: </span>Amanda specializes in 
        establishing and managing internal infrastructure and process for 
        educational institutions, research centers, and non-profits. She is 
        experienced in project management, finance, organizational leadership, 
        and program implementation, all driven by a commitment to provide 
        outstanding service.</span><br>
      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw1" style="width: 893px; top: 0px; left: 0px; display: none;">
      <!-- content for the second layer, id="sw1" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Ben Gillenhall</h3>
        <br>
        <span><span class="bold">Company: </span>Company PR</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem.
         Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>

      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw2" style="width: 893px; top: 0px; left: 0px;">
      <!-- content for the third layer, id="sw2" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Giselle Smithsonian</h3>
        <br>
        <span><span class="bold">Company: </span>Company PR</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. 
        Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw3" style="display: none; width: 893px; top: 0px; left: 0px;">
      <!-- content for the fourth layer, id="sw3" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Rafael Munez</h3>
        <br>
        <span><span class="bold">Company: </span>Audio/Visual Effects</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. 
        Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw4" style="display: none; width: 893px; top: 0px; left: 0px;">
      <!-- content for the fifth layer, id="sw4" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Brenda Wolsh</h3>
        <br>
        <span><span class="bold">Company: </span>CEO of Legal Department</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. 
        Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw5" style="display: none; width: 893px; top: 0px; left: 0px;">
      <!-- content for the sixth layer, id="sw5" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Bradley Cooper</h3>
        <br>
        <span><span class="bold">Company: </span>Human Resources</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. 
        Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
      </div>
      <div class="clear"></div>
    </div>
    <div class="circle_slider_text_wrapper" id="sw6" style="display: none; width: 893px; top: 0px; left: 0px;">
      <!-- content for the seventh layer, id="sw6" -->
      <div class="content_slider_text_block_wrap">
        <!-- "content_slider_text_block_wrap" is a div class for custom content -->
        <h3>Gillian Aston-Martin</h3>
        <br>
        <span><span class="bold">Company: </span>HTML/CSS Coder</span><br>
        <br>
        <span>Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem.
         Suspendisse commodo ullamcorper magna.Nulla sed leo. Class aptent taciti sociosqu ad litora torquent per Quisque ut turpis.
 Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
        
        </div>     
    </div>
</section>

@stop