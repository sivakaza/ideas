@extends('layouts.frontdefault')
@section('content')             
<div class="inner-banner"><div class="page-title"><h1 class="wow fadeInDown">contact us</h1></div></div>


<div class="container">
  <div class="row">
    <div class="about-page">
               <div class="content wow fadeInDown">
                 
                    
                    
          </div>
        </div>
    </div>  
</div>


<section class="contact-wrapper pad-t-30">
  <div class="container">
      <div class="row">
          <div class="contact-body">
            
 @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif      
    @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
       @endif
      <?php Session::forget('message'); ?>
    
<div class="col-md-6">
  <form role="form" name="contact" method="post" action="{{ URL::to('contact/') }}" enctype="multipart/form-data">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
      <label for="">Full Name</label>
      <input type="text" placeholder="Full Name" id="" name="name" class="form-control square">
    </div>
    <div class="form-group">
      <label for="">Your Email Address</label>
      <input type="text" placeholder="Your Email address" id="" name="email" class="form-control square">
    </div>
    <div class="form-group">
      <label for="">Your Phone Number</label>
      <input type="text" placeholder="Your Phone Number" id="" name="phone" class="form-control square">
    </div>
    <div class="form-group">
      <label for="">Subject</label>
      <input type="text" placeholder="Enter Your Subject" id="" name="subject" class="form-control square">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Message</label>
      <textarea rows="3" class="form-control square" name="message" style="height:130px;"></textarea>
    </div>
    <button class="btn btn-default square" type="submit">Submit</button>
  </form>
</div>


       <div class="col-md-4">
          <div class="title">
            <h1>Aditional info</h1>
            <span>But also the leap into electronic typesetting.</span> </div>
            
          <div class="content pad-b-10"> <img alt="location" src="img/location-icon.jpg">
            <h2>Address</h2>
            9930 Heron Meadows Drive<br>
            Houston, TX 77095
          </div>
          
          <div class="content pad-b-10"> <img alt="location" src="img/phone-icon.jpg">
            <h2>Phone</h2>
            00 000 000<br>
            00 000 000
          </div>
          
          <div class="content pad-b-10"> <img alt="location" src="img/mail-icon.jpg">
            <h2>Email</h2>
            <a href="mailto:support@rajanbusinessIdeas.com">support@rajanbusinessIdeas.com</a>
          </div>
          
          <div class="clearfix"></div>
          
          <div class="content pad-b-10"> <img alt="location" src="img/web-icon.jpg">
            <h2>Website</h2>
            <a href="www.rajanbusinessIdeas.com">www.rajanbusinessIdeas.com</a>
          </div>
        </div>
        
        
<div class="clearfix"></div>

<div class="mar-t-40">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d295941.98174263135!2d77.6309395!3d12.947286882014799!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1442666480690" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

</div>




        </div>
      </div>
  </div>
</section>


@stop