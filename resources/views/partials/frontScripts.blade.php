<!-- Bootstrap core JavaScript -->
{!! HTML::script('js/jquery-1.9.1.js') !!}
{!! HTML::script('js/bootstrap.js') !!}
{!! HTML::script('js/modernizr-2.6.2-respond-1.1.0.min.js') !!}
{!! HTML::script('js/selectmenu.js') !!}

{!! HTML::script('js/viewportchecker.js') !!}

{!! HTML::script('js/jquery_004.js') !!}
{!! HTML::script('js/jquery.easing-1.3.js') !!}

{!! HTML::script('js/prettify.js') !!}
{!! HTML::script('js/bootstrap-multiselect.js') !!}




<!-- carousel Script -->
		{!! HTML::script('js/jquery.iconmenu.js') !!}
		<script type="text/javascript">
			$(function() {
				$('#sti-menu').iconmenu();
			});
		</script>

<!-- carousel Script -->
    {!! HTML::script('js/jquery.mousewheel-3.1.12.js') !!}
    {!! HTML::script('js/jquery.jcarousellite.js') !!}

   <script type="text/javascript">
        $(function() {
            $(".bounceout .carousel").jCarouselLite({
                btnNext: ".bounceout .next",
                btnPrev: ".bounceout .prev",
                easing: "easeOutBounce",
                speed: 1000
            });
        });
		
		
    </script>
    
    <script type="text/javascript">
	(function($){
		$(document).ready(function() {
			var image_array = new Array();
			image_array = [
				{image: 'img/testimonials/1.jpg', link_url: 'img/img/testimonials/1big.jpg', link_rel: 'prettyPhoto'},
					// image for the first layer, goes with the text from id="sw0"
				{image: 'img/testimonials/2.jpg', link_url: 'img/img/testimonials/2big.jpg', link_rel: 'prettyPhoto'},
					// image for the second layer, goes with the text from id="sw1"
				{image: 'img/testimonials/3.jpg', link_url: 'img/img/testimonials/3big.jpg', link_rel: 'prettyPhoto'},
					// image for the third layer, goes with the text from id="sw2"
				{image: 'img/testimonials/4.jpg', link_url: 'img/img/testimonials/4big.jpg', link_rel: 'prettyPhoto'},
					// ...
				{image: 'img/testimonials/5.jpg', link_url: 'img/img/testimonials/5big.jpg', link_rel: 'prettyPhoto'},
				{image: 'img/testimonials/6.jpg', link_url: 'img/img/testimonials/6big.jpg', link_rel: 'prettyPhoto'},
				{image: 'img/testimonials/7.jpg', link_url: 'img/img/testimonials/7big.jpg', link_rel: 'prettyPhoto'}
			];
			
			$('#slider1').content_slider({		// bind plugin to div id="slider1"
				map : image_array,				// pointer to the image map
				max_shown_items: 7,				// number of visible circles
				hv_switch: 0,					// 0 = horizontal slider, 1 = vertical
				active_item: 0,					// layer that will be shown at start, 0=first, 1=second...
				wrapper_text_max_height: 450,	// height of widget, displayed in pixels
				middle_click: 1,				// when main circle is clicked: 1 = slider will go to the previous layer/circle, 2 = to the next
				under_600_max_height: 1200,		// if resolution is below 600 px, set max height of content
				border_radius:	-1,				// -1 = circle, 0 and other = radius
				automatic_height_resize: 1,
				border_on_off: 0,
				allow_shadow: 0
			});
		});
	})(jQuery);
</script>

  {!! HTML::script('js/wow.js') !!}  
  
  <script>
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
     document.getElementById('moar').onclick = function() {
      var section = document.createElement('section');
      section.className = 'section--purple wow fadeInDown';
      this.parentNode.insertBefore(section, this);
    };
  </script>
