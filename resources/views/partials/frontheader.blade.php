
<div class="templatemo-top-bar" id="templatemo-top">
   <div class="container">
                <div class="subheader">
                    <div class="socicons pull-left">
                        <ul>
                       	  <li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin fa-lg"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype fa-lg"></i></a></li>
                        </ul>   
                    </div>
                    <div class="pull-right">
                    	<ul class="email">
                            <li><i class="fa fa-phone fa-lg"></i> Call Us: +91-40-65656589</li>
                            <li><i class="fa fa-envelope fa-lg"></i>
                            <a href="mailto:info@support@rajanbusinessideas.com">support@rajanbusinessideas.com</a></li>
                            <li><i class="fa fa-user fa-lg"></i>
                            <a href="{{{ URL::to('auth/register') }}}">Sign Up</a></li>
                            <li class="bor-none dropdown"><i class="fa fa-sign-in fa-lg"></i>
                            <a href="{{{ URL::to('auth/login') }}}" class="dropdown-toggle" data-toggle="dropdown">Login</a>
                <div class="dropdown-menu" style="">
                  <form action="{{ url('/auth/login') }}" method="post" name="login">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <label>Username:</label>
                    <input type="text" placeholder="Email" name="email" value="" />
                    <br /><br />
                    <label>Password:</label>
                    <input type="password" name="password" value="" />
                    <br /><br />
                    <input type="submit" class="btn btn-info" value="Login" />
                  </form>
                </div>
              </li>


                    	</ul>
                    </div>
                </div>
            </div>
</div>

<div class="container">
    <nav id="navigation">
        <div class="row">
          <div class="header-v1">
            <div class="col-md-3">
              <div class="site-name"><a href="index.html"> <img src="img/logo.jpg" alt="Rajan Business Ideas"/> </a></div>
            </div>
            <div class="col-md-9">
              <div class="menu-v1">
                <div role="navigation" class="navbar">
                  <div class="navbar-header"> </div>
                  <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav pull-right">
                      <li class="active"><a href="{{{ URL::to('/') }}}">Home</a></li>
                      <li> <a href="{{{ URL::to('about') }}}">About </a>
                      </li>
                      <li><a href="{{{ URL::to('services') }}}">Services</a></li>
                      <li><a href="{{{ URL::to('businessideas') }}}">Business ideas</a></li>
                      <li><a href="{{{ URL::to('fieldreports') }}}">Field reports</a></li>
                      <li><a href="{{{ URL::to('add_contact') }}}">contact </a></li>
                    </ul>
                  </div>
                  <!--/.nav-collapse -->
                </div>
              </div>
            </div>
          </div>
        </div>
    </nav>
</div>