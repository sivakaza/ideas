<div class="clearfix"></div>

<section class="footer">
  <div class="container">
    <div class="row wow fadeInDown">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="title"><h1>Contact Us</h1></div>
        <div class="map"><img src="img/map.jpg" width="264" height="174" alt="map" class="img-responsive"></div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="address pad-t-15">
          <ul>
            <li><i class="fa fa-map-marker fa-lg"> </i>123 sed adipiscing, 
              morbiest, sagitisvel,sociosq.</li>
            <li><i class="fa fa-phone fa-lg"> </i>Phone: 0123456789</li>
            <li><i class="fa fa-envelope fa-lg"></i>Email: <a href="mailto:contact@urmailid.com">contact@urmailid.com</a></li>
            <li><i class="fa fa-dribbble fa-lg"></i><a href="">www.oursitename.com</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
        <div class="title"><h1>Newletter</h1></div>
        <p>Subscribe to our newsletter to recive news updates. free stuff and new releases by email.wen.</p>
        <div class="subscribe">
          <input name="" type="text" value="" placeholder="Enter your email address">
          <input name="Subscribe" type="submit" value="Subscribe">
        </div>
      </div>
      <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
        <div class="title"><h1>information</h1></div>
        <ul class="quicklinks pad-l-30">
          <li><a href="">Sitemap</a></li>
          <li><a href="">Privacy Policy</a></li>
          <li><a href="">Specials</a></li>
          <li><a href="">Live chat support</a></li>
          <li><a href="">eque euismod dui</a></li>
        </ul>
      </div>
    </div>
  </div>
  <footer>
    <p>© 2015 rajanbusinessideas.com. All Rights Reserved.</p>
  </footer>
</section>