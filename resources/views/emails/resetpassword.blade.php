<table width="52%" cellspacing="0" cellpadding="0" align="center" style="max-width:100%;border:1px solid #b3b3b3">
    	<tbody><tr>
   		<td bgcolor="#e80000" style="padding:15px" colspan="2">
		        <img src="https://ci3.googleusercontent.com/proxy/sK0FA9OrOWZtD4lLRK1Ey_P6_o8p_WjduWRi6aHW4ulhIr90uXCPP8A9-lGgNb_wnUOuGtiSLiAxAqJaQ-bb_Fp9U1RC=s0-d-e1-ft#http://www.oracle.com/us/assets/oracle-logo.png" class="CToWUd">
		    </td>
        </tr>

        <tr>
        	<td style="padding:32px 27px 27px 27px">
            <table style="font-size:15px;width:100%">
            	<tbody><tr><td style="padding-bottom:15px">Dear siva,</td>
                </tr><tr><td style="padding-bottom:15px">The password for your Oracle Account has been reset.</td>
                </tr><tr><td style="padding-bottom:15px">If you did not reset your password, please tell us immediately: <a target="_blank" href="http://apex.oracle.com/pls/otn/f?p=42988:3">submit a help request</a> or email <a target="_blank" href="mailto:profilehelp_ww@oracle.com">profilehelp_ww@oracle.com.</a> If you have questions, see <a target="_blank" href="http://www.oracle.com/us/corporate/contact/about-your-account-070507.html">Account Help.</a></td>
                </tr>

            </tbody></table>
			</td>


        </tr>

        <tr>
        	<td>
            <table style="font-size:15px;width:100%">

        <tbody><tr>
           <td style="padding:0px 27px 10px 27px">Thank you,</td>
        </tr>

        <tr>
           <td style="padding:0px 27px 10px 27px">The Oracle Account Team</td>
        </tr>
        	</tbody></table>
        	</td>
        </tr>

        <tr>
        	<td style="border-top:1px solid #b3b3b3">
                <table width="100%" cellspacing="0" cellpadding="0" align="center" style="padding:32px 27px 27px 27px">
    	<tbody><tr>
        	<td width="50%" align="left"><img src="https://ci5.googleusercontent.com/proxy/C7U3LVxWQH4D0y-h9-NiecpNkxN6TAh87j-N5WHiusxy8fkyDY5PiZp_QDXqBcx8CTW-o82PTUTWWW8jPJXM678jcrym4A=s0-d-e1-ft#http://www.oracle.com/us/assets/footer-logo.jpeg" class="CToWUd"></td>
            <td width="50%" align="right">
            <table>
            <tbody><tr>
            	<td><a target="_blank" style="margin-right:5px" href="http://www.oracle.com/us/social-media/facebook/index.html"><img border="0" src="https://ci4.googleusercontent.com/proxy/sUZCzLXbV0BYdpa6VbVHhv2NKUrwKZwEJxjXjg14FE9m-aBVgflt0LAdyWkCNkcVXxgYY3PtC4E68m6HX_yRLwav=s0-d-e1-ft#http://www.oracle.com/us/assets/facebook.png" class="CToWUd"></a></td>
            	<td><a target="_blank" style="margin-right:5px" href="http://www.oracle.com/us/social-media/linkedin/index.html"><img border="0" src="https://ci5.googleusercontent.com/proxy/tI8tN6rzRKUvhbnmFHqg15w1eWaSOZwbRVp_Z9uHLNU9hQsiT5NByKyvmwWcmthjxcYRATzVADjHM6j5ZKn7XLYc=s0-d-e1-ft#http://www.oracle.com/us/assets/linkedin.png" class="CToWUd"></a></td>
                <td><a target="_blank" style="margin-right:5px" href="http://www.oracle.com/us/social-media/twitter/index.html"><img border="0" src="https://ci4.googleusercontent.com/proxy/sQ0gPVJI1EhRReoAvaoSWptTQ-Dz_v02dJpq7MffVF1xLSV5jwkTy6fVdQsTCpQJXCXpdvPpSKnDPfEPaqQslNw=s0-d-e1-ft#http://www.oracle.com/us/assets/twitter.png" class="CToWUd"></a></td>
                <td><a target="_blank" style="margin-right:5px" href="https://plus.google.com/u/0/+Oracle/posts"><img border="0" src="https://ci4.googleusercontent.com/proxy/DQYNUMn5G3t-I-Q2e7aUJqpX52D7YIE0hcKizKNDRpigakFR3FwJhHO7901DJeFQXLfmFn81eTeEO6JB_swp=s0-d-e1-ft#http://www.oracle.com/us/assets/gplus.png" class="CToWUd"></a></td>
                <td><a target="_blank" style="margin-right:5px" href="http://www.youtube.com/oracle/"><img border="0" src="https://ci6.googleusercontent.com/proxy/Balg8lgRAaAKJbI8CNwDkfqLRdBvCWxfkRlrBnTFpJ0zRskUOG4wwgsHoyJFhUMSUWQwE-5WUN2uQ4-Os473XLQ=s0-d-e1-ft#http://www.oracle.com/us/assets/youtube.png" class="CToWUd"></a></td>
                <td><a target="_blank" href="http://www.oracle.com/us/syndication/feeds/index.html"><img border="0" src="https://ci5.googleusercontent.com/proxy/jQqUyKb57Az3GcgT_W-Ni0zZdLf6lmZPyZZYvdr7RRG9uMzdHTDl4A5lK5QSy1otNc-BQSCa_Cuq5cjxlcXQWBM8=s0-d-e1-ft#http://www.oracle.com/us/assets/rss-feed.png" class="CToWUd"></a></td>
            </tr>
            </tbody></table>
            	</td>
        	</tr>
    		</tbody></table>
            </td>
        </tr>

        <tr>
        	<td style="border-top:1px solid #b3b3b3">
            <table width="100%" cellspacing="0" cellpadding="0" style="padding:32px 5px 27px 5px;font-size:10px">
    	<tbody><tr>
        	<td align="left" style="width:50%">
                <table cellspacing="0" cellpadding="0" style="font-size:10px">
                <tbody><tr><td>
                Copyright @ 2015, Oracle and/or its affiliates. <br> All rights reserved.
                </td></tr>
                </tbody></table>
            </td>

            <td align="right" style="width:65%">
            	<table cellspacing="0" cellpadding="0" style="font-size:10px">
                <tbody><tr><td>
            	<a target="_blank" style="display:inline-block;margin-right:2px;padding-right:2px" href="http://www.oracle.com/us/corporate/contact/about-your-account-070507.html">Account Help</a> |
            	<a target="_blank" style="display:inline-block;margin-right:2px;padding-right:2px" href="https://dne.oracle.com/pls/uns/OPT_OUT.th?vn=MPRFL1&amp;l_code=en">Do Not Email</a> |
                <a target="_blank" style="display:inline-block;margin-right:2px;padding-right:2px" href="http://www.oracle.com/us/legal/index.html">Legal Notices</a> |
                <a target="_blank" style="display:inline-block;margin-right:2px;padding-right:2px" href="http://www.oracle.com/us/legal/terms/terms-078566.html">Terms of Use</a> |
                <a target="_blank" style="display:inline-block;margin-right:2px;padding-right:2px" href="http://www.oracle.com/us/legal/privacy/index.html">Privacy</a>


                </td>
        </tr>
    		</tbody></table>
            </td>
        </tr>

    </tbody></table>


</td></tr></tbody></table>