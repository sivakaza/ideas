@extends('app')

@section('htmlheader_title')
    Register
@endsection

@section('content')


    
        

       

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Dashboard
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{{ URL::to('permissions') }}}">Role Permission</a></li>
        <li class="active">Add Permission</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
            
            <div class="col-md-8">

       <div class="box box-widget">
                <div class="box-header with-border">
                  <div class="user-block">
                    
                    <span> <h3> <i class="fa fa-user fa-lg"></i> Add New Permission</h3> </span>
                    
                  </div><!-- /.user-block -->
                  <div class="box-tools">
                   
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                
                 @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your input.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                 @if(Session::has('message'))
                          <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                 @endif
                <?php Session::forget('message'); ?>

                <form action="{{ url('add_permission') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Permission Name" name="name" value="{{ old('name') }}"/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Display Name" name="display_name" value="{{ old('display_name') }}"/>
                    <span class="glyphicon glyphicon-cog form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Permission Description" name="description" value="{{ old('description') }}"/>
                    <span class="glyphicon glyphicon-cd form-control-feedback"></span>
                </div>
                
               

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4 pull-right">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                    </div><!-- /.col -->
                </div>
            </form>

                 
              </div>
        </div>
            
        </div><!-- /.form-box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

        
@endsection
