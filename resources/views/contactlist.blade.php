<!-- resources/views/user.blade.php -->
@extends('app')

@section('htmlheader_title')
    Register
@endsection

@section('content')


    
        

       

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Dashboard
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{ URL::to('dashboard') }}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contacts</li>
        
    </ol>
</section>
                <!-- Main content -->
<section class="content">
<div class="row">
<div class="col-md-12">

       <div class="box box-widget">
                <div class="box-header with-border">
                <div class="user-block">
                    <span><i class="glyphicon glyphicon-users"></i></span>
                    <span class="username">Contacts</span>
                    
                  </div><!-- /.user-block -->
                  <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <!-- Data Table - List USERS -->
                  <table class="table table-bordered" id="contact-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Sent Date</th>
                            </tr>
                        </thead>
                    </table>
                </div>
        </div>
</div>

    
    
</div>
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

        
   

        
    


@endsection


