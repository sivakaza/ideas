<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Rajan Business Ideas | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="{{ asset('/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- FontAwesome 4.3.0 -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons 2.0.0 -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{ asset('/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link href="{{ asset('/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="{{ asset('/plugins/iCheck/flat/blue.css') }}" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="{{ asset('/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="{{ asset('/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="{{ asset('/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <div class="wrapper">
            @include('includes.header')
            @include('includes.sidebar')
            
            @yield('content');
            

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0
                </div>
                <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Rajan Business ideas</a>.</strong> All rights reserved.
            </footer>
        </div><!-- ./wrapper -->

        <!-- jQuery 2.1.3 -->
        <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
        <!-- jQuery UI 1.11.2 -->
        <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="{{ asset('/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <!-- Datatables-->
         <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
        <!-- Morris.js charts -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="{{ asset('/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="{{ asset('/plugins/sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{ asset('/plugins/knob/jquery.knob.js') }}" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="{{ asset('/plugins/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
        <!-- datepicker -->
        <script src="{{ asset('/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
        <!-- Slimscroll -->
        <script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <!-- FastClick -->
        <script src="{{ asset('/plugins/fastclick/fastclick.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('/dist/js/app.min.js') }}" type="text/javascript"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!--<script src="{{ asset('/dist/js/pages/dashboard.js') }}" type="text/javascript"></script>-->
        <!-- AdminLTE for demo purposes -->
        <script src="{{ asset('/dist/js/demo.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
$(function() {
   var usertable = $('#users-table').DataTable({
       processing:true,
        serverSide: true,
        ajax:  '{!! route('users.data') !!}',
            // type: 'POST'
        
        
        columns: [
            { data: 'id', name: 'id' },
            { data: 'avatar', name:'avatar'},
            { data: 'firstname', name: 'firstname' },
            { data: 'lastname', name: 'lastname' },
            { data: 'email', name: 'email' },
            { data: 'dob', name: 'dob' },
            { data: 'phone', name: 'phone' },
            { data: 'role', name:'role'},
            { data: 'created_at', name:'created_at'}
        ]
    });
     
     usertable.column( 0 ).visible( false );

$('#users-table tbody').on( 'click', 'tr', function () { 
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            usertable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        
    } );
    
    $("#addUser").on('click',function(){
           window.location.href  = "{{{ URL::to('add_user')}}}";
    });

    $('#editUser').on('click', function () { 
        var rowData = usertable.rows( 'tr.selected' ).data(); 
        if(rowData.length == 0){
            alert('Please select atleast one row to Edit'); 
            return false;
        }else
           if(rowData[0]['id'] != '') //alert(rowData[0]['id']+'******');
             window.location.href = "{{{ URL::to('profile/') }}}/"+rowData[0]['id'];
             //window.open('uploads/'+rowData[0]['id'],'_blank');
             //window.location.href = 'view_resume.php?jobid='+rowData[0]['id'];
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
        //alert(table.row('tr.selected').data());//table.row('.selected').remove().draw( false );
    } );
    $('#setRole').on('click', function () { 
        var rowData = usertable.rows( 'tr.selected' ).data(); 
        if(rowData.length == 0){
            alert('Please select atleast one row to Edit'); 
            return false;
        }else
           if(rowData[0]['id'] != '') //alert(rowData[0]['id']+'******');
             //window.open('uploads/'+rowData[0]['id'],'_blank');
             window.location.href = "{{{ URL::to('assign_role/') }}}/"+rowData[0]['id'];
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
        //alert(table.row('tr.selected').data());//table.row('.selected').remove().draw( false );
    } );
    $('#deleteUser').on('click', function () {
        
        var rowData = usertable.rows( 'tr.selected' ).data();
        if(rowData.length == 0){
            alert('Please select atleast one row to Delete');   
            return false;
        }else{
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
             if(rowData[0]['id'] != '')
             {
                
                 $.get('{{{ URL::to('delete_user')}}}/'+rowData[0]['id'], function(o) {
                        if(o['error'] == false){
                           usertable.row('.selected').remove().draw( false );
                           alert('User has been deleted successfully'); 
                        }else alert('Unable to delete the requested User');  
                        console.log(o);//delItem.parent().remove();
                    }, 'json');
             }//if end
        }
            return false;
        
    });

});


        </script>

       
<script type="text/javascript">
$(function() {
   var  roletable = $('#roles-table').DataTable({
        processing:true,
        serverSide: true,
        ajax:  '{!! route('role.data') !!}',
            // type: 'POST'
        
        
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name:'name'},
            { data: 'display_name', name: 'display_name' },
            { data: 'description', name: 'description' },
            { data: 'created_at', name:'created_at'}
        ]
    });
     
     roletable.column( 0 ).visible( false );

$('#roles-table tbody').on( 'click', 'tr', function () 
{ 
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            roletable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        
});
    
    $("#addRole").on('click',function(){
           window.location.href  = "{{{ URL::to('add_role')}}}";
    });

    $('#editRole').on('click', function () { 
        var rowData = roletable.rows( 'tr.selected' ).data(); 
        if(rowData.length == 0){
            alert('Please select atleast one row to Edit'); 
            return false;
        }else
           if(rowData[0]['id'] != '') //alert(rowData[0]['id']+'******');
             window.location.href = "{{{ URL::to('edit_role/') }}}/"+rowData[0]['id'];
             //window.open('uploads/'+rowData[0]['id'],'_blank');
             //window.location.href = 'view_resume.php?jobid='+rowData[0]['id'];
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
        //alert(table.row('tr.selected').data());//table.row('.selected').remove().draw( false );
    } );
    $('#setRolePermissions').on('click', function () { 
        var rowData = roletable.rows( 'tr.selected' ).data(); 
        if(rowData.length == 0){
            alert('Please select atleast one row to Edit'); 
            return false;
        }else
           if(rowData[0]['id'] != '') //alert(rowData[0]['id']+'******');
             //window.open('uploads/'+rowData[0]['id'],'_blank');
             window.location.href = "{{{ URL::to('assign_permission')}}}/"+rowData[0]['id'];
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
        //alert(table.row('tr.selected').data());//table.row('.selected').remove().draw( false );
    } );
    $('#deleteRole').on('click', function () {
        
        var rowData = roletable.rows( 'tr.selected' ).data();
        if(rowData.length == 0){
            alert('Please select atleast one row to Delete');   
            return false;
        }else{
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
             if(rowData[0]['id'] != '')
             {
                
                 $.get('{{{ URL::to('delete_role')}}}/'+rowData[0]['id'], function(o) {
                        if(o['error'] == false){
                           roletable.row('.selected').remove().draw( false );
                           alert('Role has been deleted successfully'); 
                        }else alert('Unable to delete the requested Role');  
                        console.log(o);//delItem.parent().remove();
                    }, 'json');
             }//if end
        }
            return false;
        
    });

});


        </script>

<script type="text/javascript">
$(function() {
   var  permissiontable = $('#permission-table').DataTable({
        processing:true,
        serverSide: true,
        ajax:  '{!! route('permissions.data') !!}',
            // type: 'POST'
        
        
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name:'name'},
            { data: 'display_name', name: 'display_name' },
            { data: 'description', name: 'description' },
            { data: 'created_at', name:'created_at'}
        ]
    });
     
     permissiontable.column( 0 ).visible( false );

$('#permission-table tbody').on( 'click', 'tr', function () 
{ 
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            permissiontable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        
});
    
    $("#addPermission").on('click',function(){
           window.location.href  = "{{{ URL::to('add_permission')}}}";
    });

    $('#editPermission').on('click', function () { 
        var rowData = permissiontable.rows( 'tr.selected' ).data(); 
        if(rowData.length == 0){
            alert('Please select atleast one row to Edit'); 
            return false;
        }else
           if(rowData[0]['id'] != '') //alert(rowData[0]['id']+'******');
             window.location.href = "{{{ URL::to('edit_permission/') }}}/"+rowData[0]['id'];
             //window.open('uploads/'+rowData[0]['id'],'_blank');
             //window.location.href = 'view_resume.php?jobid='+rowData[0]['id'];
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
        //alert(table.row('tr.selected').data());//table.row('.selected').remove().draw( false );
    } );
    $('#setRolePermissions').on('click', function () { 
        var rowData = permissiontable.rows( 'tr.selected' ).data(); 
        if(rowData.length == 0){
            alert('Please select atleast one row to Edit'); 
            return false;
        }else
           if(rowData[0]['id'] != '') alert(rowData[0]['id']+'******');
             //window.open('uploads/'+rowData[0]['id'],'_blank');
             //window.location.href = 'view_resume.php?jobid='+rowData[0]['id'];
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
        //alert(table.row('tr.selected').data());//table.row('.selected').remove().draw( false );
    });
    $('#deletePermission').on('click', function () {
        
        var rowData = permissiontable.rows( 'tr.selected' ).data();
        if(rowData.length == 0){
            alert('Please select atleast one row to Delete');   
            return false;
        }else{
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
             if(rowData[0]['id'] != '')
             {
                
                 $.get('{{{ URL::to('delete_permission')}}}/'+rowData[0]['id'], function(o) {
                        if(o['error'] == false){
                           permissiontable.row('.selected').remove().draw( false );
                           alert('Permission has been deleted successfully'); 
                        }else alert('Unable to delete the requested Permission');  
                        console.log(o);//delItem.parent().remove();
                    }, 'json');
             }//if end
        }
            return false;
        
    });

});


</script>
<script type="text/javascript">
$(function() {
   var  contacttable = $('#contact-table').DataTable({
        processing:true,
        serverSide: true,
        ajax:  '{!! route('contact.data') !!}',
            // type: 'POST'
        
        
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name:'name'},
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            { data: 'subject', name: 'subject' },
            { data: 'message', name: 'message' },
            { data: 'created_at', name:'created_at'}
        ]
    });
     
    contacttable.column( 0 ).visible( false );

    $('#contact-table tbody').on( 'click', 'tr', function () 
    { 
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                contacttable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            
    });
    
    
});


        </script>
        <script>
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        //CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
      });
    </script>
<script type="text/javascript">
$(function() {
   var  sectortable = $('#sector-table').DataTable({
        processing:true,
        serverSide: true,
        ajax:  '{!! route('sectors.data') !!}',
            // type: 'POST'
        
        
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name:'name'},
            { data: 'display_name', name: 'display_name' },
            { data: 'description', name: 'description' },
            { data: 'created_at', name:'created_at'}
        ]
    });
     
     sectortable.column( 0 ).visible( false );

$('#sector-table tbody').on( 'click', 'tr', function () 
{ 
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            sectortable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        
});
    
    $("#addSector").on('click',function(){
           window.location.href  = "{{{ URL::to('add_sector')}}}";
    });

    $('#editSector').on('click', function () { 
        var rowData = sectortable.rows( 'tr.selected' ).data(); 
        if(rowData.length == 0){
            alert('Please select atleast one row to Edit'); 
            return false;
        }else
           if(rowData[0]['id'] != '') //alert(rowData[0]['id']+'******');
             window.location.href = "{{{ URL::to('edit_sector/') }}}/"+rowData[0]['id'];
            
    } );
    $('#deleteSector').on('click', function () {
        
        var rowData = sectortable.rows( 'tr.selected' ).data();
        if(rowData.length == 0){
            alert('Please select atleast one row to Delete');   
            return false;
        }else{
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
             if(rowData[0]['id'] != '')
             {
                
                 $.get('{{{ URL::to('delete_sector')}}}/'+rowData[0]['id'], function(o) {
                        if(o['error'] == false){
                           sectortable.row('.selected').remove().draw( false );
                           alert('Sector has been deleted successfully'); 
                        }else alert('Unable to delete the requested Sector');  
                        console.log(o);//delItem.parent().remove();
                    }, 'json');
             }//if end
        }
            return false;
        
    });

});


        </script>
<script type="text/javascript">
$(function() {
   var  industrytable = $('#industry-table').DataTable({
        processing:true,
        serverSide: true,
        ajax:  '{!! route('industries.data') !!}',
            // type: 'POST'
        
        
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name:'name'},
            { data: 'display_name', name: 'display_name' },
            { data: 'description', name: 'description' },
            { data: 'created_at', name:'created_at'}
        ]
    });
     
     industrytable.column( 0 ).visible( false );

$('#industry-table tbody').on( 'click', 'tr', function () 
{ 
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            industrytable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        
});
    
    $("#addIndustry").on('click',function(){
           window.location.href  = "{{{ URL::to('add_industry')}}}";
    });

    $('#editIndustry').on('click', function () { 
        var rowData = industrytable.rows( 'tr.selected' ).data(); 
        if(rowData.length == 0){
            alert('Please select atleast one row to Edit'); 
            return false;
        }else
           if(rowData[0]['id'] != '') //alert(rowData[0]['id']+'******');
             window.location.href = "{{{ URL::to('edit_industry/') }}}/"+rowData[0]['id'];
            
    } );
    $('#deleteIndustry').on('click', function () {
        
        var rowData = industrytable.rows( 'tr.selected' ).data();
        if(rowData.length == 0){
            alert('Please select atleast one row to Delete');   
            return false;
        }else{
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
             if(rowData[0]['id'] != '')
             {
                
                 $.get('{{{ URL::to('delete_industry')}}}/'+rowData[0]['id'], function(o) {
                        if(o['error'] == false){
                           industrytable.row('.selected').remove().draw( false );
                           alert('Industry has been deleted successfully'); 
                        }else alert('Unable to delete the requested Industry');  
                        console.log(o);//delItem.parent().remove();
                    }, 'json');
             }//if end
        }
            return false;
        
    });

});


        </script>

<script type="text/javascript">
$(function() {
   var  ideatable = $('#idea-table').DataTable({
        processing:true,
        serverSide: true,
        ajax:  '{!! route('ideas.data') !!}',
            // type: 'POST'
        
        
        columns: [
            { data: 'id', name: 'id' },
            { data: 'category', name:'category'},
            { data: 'title', name: 'title' },
            { data: 'short_description', name: 'short_description' },
            { data: 'price', name: 'price' },
            { data: 'approximate_budget', name: 'approximate_budget' },
            { data: 'success_probability', name: 'success_probability' },
            { data: 'location', name: 'location' },
            { data: 'created_by', name: 'created_by' },
            { data: 'updated_at', name:'created_at'}
        ]
    });
     
     ideatable.column( 0 ).visible( false );

$('#idea-table tbody').on( 'click', 'tr', function () 
{ 
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            ideatable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        
});
    
    $("#addIdea").on('click',function(){
           window.location.href  = "{{{ URL::to('add_idea')}}}";
    });

    $('#editIdea').on('click', function () { 
        var rowData = ideatable.rows( 'tr.selected' ).data(); 
        if(rowData.length == 0){
            alert('Please select atleast one row to Edit'); 
            return false;
        }else
           if(rowData[0]['id'] != '') //alert(rowData[0]['id']+'******');
             window.location.href = "{{{ URL::to('edit_idea/') }}}/"+rowData[0]['id'];
            
    } );
    $('#deleteIdea').on('click', function () {
        
        var rowData = ideatable.rows( 'tr.selected' ).data();
        if(rowData.length == 0){
            alert('Please select atleast one row to Delete');   
            return false;
        }else{
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
             if(rowData[0]['id'] != '')
             {
                
                 $.get('{{{ URL::to('delete_idea')}}}/'+rowData[0]['id'], function(o) {
                        if(o['error'] == false){
                           ideatable.row('.selected').remove().draw( false );
                           alert('Idea has been deleted successfully'); 
                        }else alert('Unable to delete the requested Industry');  
                        console.log(o);//delItem.parent().remove();
                    }, 'json');
             }//if end
        }
            return false;
        
    });

     $('#MailList').on('click', function () {
        
        var rowData = ideatable.rows( 'tr.selected' ).data();
        if(rowData.length == 0){
            alert('Please select atleast one row to add MailList');   
            return false;
        }else{
         //console.log(rowData[0]['login']+rowData[0]['rowRef']);
             if(rowData[0]['id'] != '')
             {
                window.location.href ='{{{ URL::to('idea')}}}/'+rowData[0]['id']+"/mail_list"
                 // $.get('{{{ URL::to('delete_idea')}}}/'+rowData[0]['id'], function(o) {
                 //        if(o['error'] == false){
                 //           ideatable.row('.selected').remove().draw( false );
                 //           alert('Idea has been deleted successfully'); 
                 //        }else alert('Unable to delete the requested Idea');  
                 //        console.log(o);//delItem.parent().remove();
                 //    }, 'json');
             }//if end
        }
            return false;
        
    });


});


        </script>


    </body>
</html>