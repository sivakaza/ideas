<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessIdea extends Model
{
    //


     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ideas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','category','title', 'price', 'short_description', 'description', 'segments', 'success_probability', 'approximate_budget', 'image', 'files', 'created_by', 'status', 'mail_listing', 'location'];
}
