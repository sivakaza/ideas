<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Contact extends Model 
{
    
 // public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'email', 'phone', 'subject', 'message','created_at', 'updated_at'];

       
}
