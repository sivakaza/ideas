<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('about', function () {
    return view('about');
});
Route::get('services', function () {
    return view('services');
});

//Contact Us
Route::get('add_contact', function () {
    return view('contact');
});
Route::post('contact', 'ContactController@submitcontact');
Route::controller('contact', 'ContactController', [
     'anyData'  => 'contact.data',
     'getIndex' => 'contact',
 ]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

//Profile Edit
Route::get('profile/{id}', ['middleware' => 'auth', 'uses' => 'profileController@edit']);
Route::post('profile/{id}', ['middleware' => 'auth', 'uses' => 'profileController@store']);

//Uers
Route::controller('users', 'UserController', [
     'anyData'  => 'users.data',
     'getIndex' => 'users',
 ]);
Route::get('add_user', 'UserController@create');
Route::post('add_user','UserController@addUser');
Route::get('delete_user/{id}','UserController@destroy');
Route::get('assign_role/{id}','UserController@assignUserRole');
Route::post('assign_role/{id}','UserController@setUserRole');

//Roles
Route::controller('role', 'RoleController', [
     'anyData'  => 'role.data',
     'getIndex' => 'role',
 ]);
Route::get('add_role', function(){
   return view('addrole');
});
Route::post('add_role','RoleController@addRole');
Route::get('edit_role/{id}','RoleController@edit');
Route::post('edit_role/{id}','RoleController@store');
Route::get('delete_role/{id}','RoleController@destroy');
Route::get('assign_permission/{id}','RoleController@assignRolePermission');
Route::post('assign_permission/{id}','RoleController@setRolePermission');


//permissions
Route::controller('permissions', 'PermissionController', [
     'anyData'  => 'permissions.data',
     'getIndex' => 'permissions',
 ]);
Route::get('add_permission', function(){
   return view('addpermission');
});
Route::post('add_permission','PermissionController@addPermission');
Route::get('edit_permission/{id}','PermissionController@edit');
Route::post('edit_permission/{id}','PermissionController@store');
Route::get('delete_permission/{id}','PermissionController@destroy');


Route::get('dashboard', function () { 
   return view('dashboard');
});



/**
 * Sectors for the BI
 *
 **/
Route::controller('sectors', 'SectorController', [
     'anyData'  => 'sectors.data',
     'getIndex' => 'sectors',
 ]);
Route::get('add_sector', 'SectorController@create');
Route::post('store_sector','SectorController@store');
Route::get('edit_sector/{id}','SectorController@edit');
Route::post('update_sector/{id}','SectorController@update');
Route::get('delete_sector/{id}','SectorController@destroy');



/**
 *  Business ideas  
 **/
Route::get('add_idea', 'BusinessIdeaController@create');
Route::post('store_idea', 'BusinessIdeaController@store');
Route::get('edit_idea/{id}', 'BusinessIdeaController@edit');
Route::post('update_idea/{id}', 'BusinessIdeaController@update');
Route::get('delete_idea/{id}', 'BusinessIdeaController@destroy');
Route::get('businessideas','BusinessIdeaController@index');
Route::get('businessidea/download/{name?}','BusinessIdeaController@downloadfile')->after('auth');
Route::get('idea/{id}/mail_list',"BusinessIdeaController@createMailList");
Route::post('idea/{id}/mail_list',"BusinessIdeaController@storeMailList");

Route::controller('ideas', 'BusinessIdeaController', [
     'anyData'  => 'ideas.data',
     'getIndex' => 'ideas',
 ]);
/**
 *  Business ideas  End
 **/



/**
 * Industries for the FR
 *
 **/
Route::controller('industries', 'IndustryController', [
     'anyData'  => 'industries.data',
     'getIndex' => 'industries',
 ]);
Route::get('add_industry', 'IndustryController@create');
Route::post('store_industry','IndustryController@store');
Route::get('edit_industry/{id}','IndustryController@edit');
Route::post('update_industry/{id}','IndustryController@update');
Route::get('delete_industry/{id}','IndustryController@destroy');


/*
 *  Field Reports
 **/
Route::get('add_report', 'FieldReportController@create');
Route::get('fieldreports', function(){
     return view('fieldreports.fieldreports');
});

/*
 *  Field Reports  end
 **/

//Sample to routes for checkin or test
//Route::get('dashboard/', "DashboardController@index");
Route::get('test', function()
{
    dd(Config::get('mail'));
});
Route::get('testmail',function(){
   Mail::raw('Laravel with Mailgun is easy!', function($message)
	{
	    $message->to('sivakazaa@gmail.com');
	});
});