<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use Validator;

use yajra\Datatables\Datatables;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(){ 
    //
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
        
        return \View::make('role');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        //return Datatables::of(User::select('*'))->make(true);
        $roles = Role::select(['id', 'name', 'display_name', 'description', 'created_at']);

        return Datatables::of($roles)
           ->editColumn('created_at',function($data){
                return  date('F d, Y', strtotime($data->created_at));
             })
           ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //
        if(\Input::get('id')){  
             
             $validator = $this->validator(\Input::all());
             if ($validator->fails()) { 
                
                // get the error messages from the validator
                $messages = $validator->messages(); 
                return redirect('edit_role/'.\Input::get('id'))
                            ->withErrors($validator)
                            ->withInput();
            }
            
            

            $role = Role::find(\Input::get('id'));
            $role->name = \Input::get('name');
            $role->display_name = \Input::get('display_name');
            $role->description = \Input::get('description');
            
            if($role->save()){ 
           \Session::flash('message', 'Role has been Updated successfully'); 
           \Session::flash('alert-class', 'alert-info'); }

           return \View::make('editrole')->with('role',$role);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $role = Role::find($id); 
        // Load user/createOrUpdate.blade.php view
        return \View::make('editrole')->with('role', $role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:roles,name,'.\Input::get('id'),
            'display_name' => 'required|max:255',
            
        ]);
    }


    /**
    *   Add user from backend
    *   @param : User Data
    *   @return : 
    */

    protected function addRole(){
         
         $validator = $this->Validator(\Input::all());
         if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();

            return redirect('add_role')
                        ->withErrors($validator)
                        ->withInput();
        }
       
         $data = \Input::all();
         $role = Role::create(['name' => $data['name'],
                                'display_name' => $data['display_name'],
                                'description' => $data['description']
                                ]); 
          if($role){
           \Session::flash('message', 'Role has been added successfully'); 
           \Session::flash('alert-class', 'alert-info'); 
 

           

           return redirect('edit_role/'.$role->id);
         }//else{ echo 'Failed';}

    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
       $role = Role::find($id);
       if($role->delete()){
          return \Response::json(array(
                        'error' => false,
                        'message' => 'Role has been deleted Successfully !!!'),
                        200
                    );
       }
    }

    /**
     *  Show form to assign role permissions
     *
     **/
     public function assignRolePermission($roleid)
     {
         # code...
         return view('user.assignpermissions', ['permissions' => Permission::all(), 
                                                'role'=>Role::find($roleid), 
                                                'existingPermissions'=>$this->getAssignedPermissionIds($roleid)
                                     ]);
     }

     /**
     *  Assign the permissions to the perticular role
     *
     **/
     public function setRolePermission($roleid)
     {
         # code...
        # code...
         $role = Role::find(\Input::get('id'));

         $selectedPermissions = \Input::get('permission');
         
         $role->permissions()->detach();
         
         foreach($selectedPermissions as $selectedPermission ){
             $role->attachPermission($selectedPermission);
        }

        \Session::flash('message', 'Permission(s) has been assigned successfully'); 

         return view('user.assignpermissions', ['permissions' => Permission::all(), 
                                                 'role'=>$role, 
                                                 'existingPermissions'=>$this->getAssignedPermissionIds($roleid)
                                             ]);
     }


     /**
     * Get the assigned permissionsId.
     *
     * @return array of assigned role ids for the given userid
     */
    public function getAssignedPermissionIds($roleId)
    {
        # code...
         $permissionids = array();
         $rolepermissions = Role::find($roleId)->permissions; 

        if($rolepermissions)
          foreach($rolepermissions as $rolepermission)
                    $permissionids[]= $rolepermission->id;

         return $permissionids;
    }

}
