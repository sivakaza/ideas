<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permission;
use Validator;

use yajra\Datatables\Datatables;


class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(){ 
    //
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
        
        return \View::make('permission');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        //return Datatables::of(User::select('*'))->make(true);
        $permission = Permission::select(['id', 'name', 'display_name', 'description', 'created_at']);

        return Datatables::of($permission)
           ->editColumn('created_at',function($data){
                return  date('F d, Y', strtotime($data->created_at));
             })
           ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //
        if(\Input::get('id')){  
             
             $validator = $this->validator(\Input::all());
             if ($validator->fails()) { 
                
                // get the error messages from the validator
                $messages = $validator->messages(); 
                return redirect('edit_role/'.\Input::get('id'))
                            ->withErrors($validator)
                            ->withInput();
            }
            
            

            $permission = Permission::find(\Input::get('id'));
            $permission->name = \Input::get('name');
            $permission->display_name = \Input::get('display_name');
            $permission->description = \Input::get('description');
            
            if($permission->save()){ 
           \Session::flash('message', 'Permission has been Updated successfully'); 
           \Session::flash('alert-class', 'alert-info'); }

           return \View::make('editpermission')->with('permission',$permission);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $permission = Permission::find($id); 
        // Load user/createOrUpdate.blade.php view
        return \View::make('editpermission')->with('permission', $permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

       /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:permissions,name,'.\Input::get('id'),
            'display_name' => 'required|max:255',
            
        ]);
    }


    /**
    *   Add user from backend
    *   @param : User Data
    *   @return : 
    */

    protected function addPermission(){
         
         $validator = $this->Validator(\Input::all());
         if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();

            return redirect('add_permission')
                        ->withErrors($validator)
                        ->withInput();
        }
       
         $data = \Input::all();
         $permission = Permission::create(['name' => $data['name'],
                                'display_name' => $data['display_name'],
                                'description' => $data['description']
                                ]); 
          if($permission){
           \Session::flash('message', 'Permission has been added successfully'); 
           \Session::flash('alert-class', 'alert-info'); 
 

           

           return redirect('edit_permission/'.$permission->id);
         }//else{ echo 'Failed';}

    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
       $permission = Permission::find($id);
       if($permission->delete()){
          return \Response::json(array(
                        'error' => false,
                        'message' => 'Permission has been deleted Successfully !!!'),
                        200
                    );
       }
    }

}
