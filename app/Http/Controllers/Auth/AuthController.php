<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Mail;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Request;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    
     
     /**
     * Used to redirect the user after the passing the authentication
     *
     */
     protected $redirectPath = '/dashboard';
     
    public $avtar = '';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'dob' => 'required|date',
            'phone' => 'required|digits:10',//phone:US,BE,IN',//require "propaganistas/laravel-phone": "~2.0"
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {  
         
        
        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'dob' => $data['dob'],
            'phone' => $data['phone'],
            'website' => '',
            'avatar' => $this->avtar
        ]);
    }

    public function handleAvtar(){
    // $file = $request->file('image'); or
    // $fileName = 'somename';
        $destinationPath = 'images/';
        \Input::file('image')->move($destinationPath);
     }
    

    protected function postRegister(){
        //echo 'in controller postregister'; exit;

        // $validator =Validator::make(\Input::all(), [
            
        //     'email' => 'required|email|max:255|unique:users',
        //     'password' => 'required|confirmed|min:6',
        //     'firstname' => 'required|max:255',
        //     'lastname' => 'required|max:255',
        //     'dob' => 'required|date',
        //     'phone' => 'required|phone:US,BE,IN',//require "propaganistas/laravel-phone": "~2.0"
            
        // ]);
         
         $validator = $this->validator(\Input::all());
         if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();

            return redirect('auth/register')
                        ->withErrors($validator)
                        ->withInput();
        }
       
       if (Request::hasFile('avtar')) {
             $file = Request::file('avtar');
            $image_name = time()."-".$file->getClientOriginalName();
            $file->move('uploads/avtar/', $image_name);
            $image = \Image::make(sprintf('uploads/avtar/%s', $image_name))->resize('160','160')->save('uploads/avtar/resized/'.$image_name);
            $this->avtar = 'uploads/avtar/'.$image->basename;
        }

        $user = $this->create(\Input::all());
          if($user){
           \Session::flash('message', 'Account has been added successfully'); 
           \Session::flash('alert-class', 'alert-info'); 

           $user->attachRole(8);

            $emails = ['sivakazaa@gmail.com', 'siva09.uit@gmail.com','info@rajanbusinessideas.com'];

            Mail::send('emails.registration', \Input::all(), function($message) use ($emails)
            {    
                $message->to($emails)
                        ->from('info@rajanbusinessideas.com', 'Rajan Business Ideas')
                        ->subject('New Usere Registration in Rajan Business Ideas');    
            });

           return \View::make('auth.login');
         }//else{ echo 'Failed';}

    }

   

}
