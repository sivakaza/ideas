<?php namespace App\Http\Controllers\Auth;

use DB;
use Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Illuminate\Http\Request;

class ApiUserController extends Controller {

	
	
	// public function __construct()
	// {
	// 	 $this->middleware('auth'); 
	// } 


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$users = User::all(['id','firstname','lastname', 'phone', 'dob', 'website', 'email']);
		return \Response::json(array(
				        'error' => false,
				        'users' => $users->toArray()),
				        200
				    );
		
	}
	/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'dob' => 'required|date',
            'phone' => 'required|phone:US,BE,IN',//require "propaganistas/laravel-phone": "~2.0"
            
        ]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $user	=\Request::all();
	    $validator = $this->validator($user);

	    if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();
            return \Response::json(array(
				        'error' => true,
				        'message' => $messages),
				        422
				    );
            
        }
        if(User::create(\Request::all()))
        	return \Response::json(array(
				        'error' => false,
				        'message' => 'User added successfully'),
				        200
				    );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$user = User::find($id);
		if(!isset($user))
			return \Response::json(array(
				        'error' => true,
				        'message' => 'User Not Exist'),
				        422
				    );
	    else
	    	return \Response::json(array(
				        'error' => false,
				        'message' => $user->toArray()),
				        200
				    );
		 
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$data = \Request::all();
		$validator = Validator::make($data, [
            'email' => 'required|email|max:255|unique:users,email,'.\Request::get('id'),
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'dob' => 'required|date',
            'phone' => 'required|phone:US,BE,IN',//require "propaganistas/laravel-phone": "~2.0"
            
        ]);
        if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();
            return \Response::json(array(
				        'error' => true,
				        'message' => $messages),
				        422
				    );
            
        }
            $user = User::find($id);
            $user->email = \Request::get('email');
            $user->firstname = \Request::get('firstname');
            $user->lastname = \Request::get('lastname');
            $user->phone = \Request::get('phone');
            $user->dob = \Request::get('dob');
            $user->website = \Request::get('website');
            if($user->save())
            	return \Response::json(array(
				        'error' => false,
				        'message' => 'User updated successfully'),
				        400
				    );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		
	}

}
