<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;

use yajra\Datatables\Datatables;
/**
* DashboardController
*/
class DashboardController extends Controller
{
	
	
	public function getIndex(){ 
	//
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
        
		return \View::make('user');
	}

	/**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        //return Datatables::of(User::select('*'))->make(true);
        $users = User::select(['id', 'firstname', 'lastname', 'email', 'dob', 'phone']);

        return Datatables::of($users)
           ->addColumn('role', function ($user) {
           	     $rolename = array();
                 $userRoles = User::find($user->id)->roles;
                 foreach($userRoles as $userRole)
                    $rolename[]= $userRole->name;
                return implode(', ', $rolename);
            })
            ->make(true);
    }
}