<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
class profileController extends Controller
{

    /**
    *  Profile Avtar
    */
    public $avtar = '';
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users,email,'.\Input::get('id'),
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'dob' => 'required|date',
            'phone' => 'required|digits:10',//require "propaganistas/laravel-phone": "~2.0"
            
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(\Request $request)
    {
        //
       
        if(\Input::get('id')){  
             
             $validator = $this->validator(\Input::all());
             if ($validator->fails()) { 
                
                // get the error messages from the validator
                $messages = $validator->messages(); 
                return redirect('profile/'.\Input::get('id'))
                            ->withErrors($validator)
                            ->withInput();
            }
            
            if (\Request::hasFile('avtar')) 
            {
                $file = \Request::file('avtar');
                $image_name = time()."-".$file->getClientOriginalName();
                $file->move('uploads/avtar/', $image_name);
                $image = \Image::make(sprintf('uploads/avtar/%s', $image_name))->resize('160','160')->save('uploads/avtar/resized/'.$image_name);
                $this->avtar = 'uploads/avtar/'.$image->basename;
            }

            $user = User::find(\Input::get('id'));
            $user->email = \Input::get('email');
            $user->firstname = \Input::get('firstname');
            $user->lastname = \Input::get('lastname');
            $user->phone = \Input::get('phone');
            $user->dob = \Input::get('dob');
            //$user->website = \Input::get('website');
            $user->avatar = ($this->avtar) ? $this->avtar : \Input::get('avtar_previous');   

            if($user->save()){ 
           \Session::flash('message', 'Account has been Updated successfully'); 
           \Session::flash('alert-class', 'alert-info'); }

           return \View::make('profile.editProfile')->with('user',$user);
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id); 
        // Load user/createOrUpdate.blade.php view
        return \View::make('profile.editProfile')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
