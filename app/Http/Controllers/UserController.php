<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Validator;
use Request;
use Mail;
use yajra\Datatables\Datatables;

/**
* DashboardController
*/
class UserController extends Controller
{
	
    public $avtar = '';
	
	public function getIndex(){ 
	//
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
        
		return \View::make('user');
	}

	/**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        //return Datatables::of(User::select('*'))->make(true);
        $users = User::select(['id', 'avatar', 'firstname', 'lastname', 'email', 'dob', 'phone', 'created_at']);

        return Datatables::of($users)
           ->editColumn('avatar',function($user){
                 $default_avatar = 'img/avatar5.png';
                return $user->avatar = ($user->avatar) ? '<img src="'.  $user->avatar .'" class="img-circle" style="height:25px; width:25px;">' : '<img src="'.  $default_avatar.'" class="img-circle" style="height:25px; width:25px;">';
            })
           ->editColumn('created_at',function($user){
                return  date('F d, Y', strtotime($user->created_at));
             })
           ->addColumn('role', function ($user) {
           	     $rolename = array();
                 $userRoles = User::find($user->id)->roles;
                 foreach($userRoles as $userRole)
                    $rolename[]= $userRole->name;
                return implode(', ', $rolename);
             })
            ->make(true);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'role'  => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'dob' => 'required|date',
            'phone' => 'required|digits:10',//phone:US,BE,IN',//require "propaganistas/laravel-phone": "~2.0"
            
        ]);
    }


    /**
    *   Add user from backend
    *   @param : User Data
    *   @return : 
    */

    protected function addUser(){
         
         $validator = $this->Validator(\Input::all());
         if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();

            return redirect('add_user')
                        ->withErrors($validator)
                        ->withInput();
        }
       
       if (Request::hasFile('avtar')) {
             $file = Request::file('avtar');
            $image_name = time()."-".$file->getClientOriginalName();
            $file->move('uploads/avtar/', $image_name);
            $image = \Image::make(sprintf('uploads/avtar/%s', $image_name))->resize('160','160')->save('uploads/avtar/resized/'.$image_name);
            $this->avtar = 'uploads/avtar/'.$image->basename;
        }
         $data = \Input::all();
        $user = User::create(['email' => $data['email'],
                                'password' => bcrypt($data['password']),
                                'firstname' => $data['firstname'],
                                'lastname' => $data['lastname'],
                                'dob' => $data['dob'],
                                'phone' => $data['phone'],
                                'website' => '',
                                'avatar' => $this->avtar
                             ]); 
          if($user){
           \Session::flash('message', 'Account has been added successfully'); 
           \Session::flash('alert-class', 'alert-info'); 
 

           $user->attachRole($data['role']);

           $emails = ['sivakazaa@gmail.com', 'siva09.uit@gmail.com','info@rajanbusinessideas.com'];

            Mail::send('emails.registration', $data, function($message) use ($emails)
            {    
                $message->to($emails)
                        ->from('info@rajanbusinessideas.com', 'Rajan Business Ideas')
                        ->subject('New Usere Registration in Rajan Business Ideas');    
            });

           return redirect('profile/'.$user->id);
         }//else{ echo 'Failed';}

    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
       $user = User::find($id);
       if($user->delete()){
          return \Response::json(array(
                        'error' => false,
                        'message' => 'User has been deleted Successfully !!!'),
                        200
                    );
       }
    }

     /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create($value='')
    {
        # code...
        $roles = Role::all(); 
        return view('user.adduser', ['roles' => $roles]);
    }


    public function assignUserRole($userId)
    {
        # code...
        
         return view('user.assignrole', ['roles' => Role::all(), 
                                         'user'=>User::find($userId), 
                                         'existingRoles'=>$this->getAssignedRoleIds($userId)
                                     ]);

    }

    public function setUserRole(\Request $request)
    {
        # code...
         $user = User::find(\Input::get('id'));

         $selectedRoles = \Input::get('role');
         
         $user->roles()->detach();
         
         foreach($selectedRoles as $selectedRole ){
             $user->attachRole($selectedRole);
        }

        \Session::flash('message', 'Role(s) has been assigned successfully'); 

         return view('user.assignrole', ['roles' => Role::all(), 
                                         'user'=>$user, 
                                         'existingRoles'=>$this->getAssignedRoleIds(\Input::get('id'))
                                     ]);
    }
   
    /**
     * Get the assigned roleId.
     *
     * @return array of assigned role ids for the given userid
     */
    public function getAssignedRoleIds($userId)
    {
        # code...
         $roleids = array();
         $userRoles = User::find($userId)->roles;
         foreach($userRoles as $userRole)
                    $roleids[]= $userRole->id;

         return $roleids;
    }

}