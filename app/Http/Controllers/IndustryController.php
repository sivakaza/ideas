<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Industry;
use App\Http\Controllers\Controller;
use Validator;

use yajra\Datatables\Datatables;

class IndustryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(){ 
    //
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
        
        return \View::make('fieldreports.industries');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        //return Datatables::of(User::select('*'))->make(true);
        $industry = Industry::select(['id', 'name', 'display_name', 'description', 'created_at']);

        return Datatables::of($industry)
           ->editColumn('created_at',function($data){
                return  date('F d, Y', strtotime($data->created_at));
             })
           ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         return \View::make('fieldreports.addindustry');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:industries,name,'.\Input::get('id'),
            'display_name' => 'required|max:255',
            
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $validator = $this->Validator(\Input::all());
         if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();

            return redirect('fieldreports.add_industry')
                        ->withErrors($validator)
                        ->withInput();
        }
       
         $data = \Input::all();
         $industry = Industry::create(['name' => $data['name'],
                                'display_name' => $data['display_name'],
                                'description' => $data['description']
                                ]); 
          if($industry){
           \Session::flash('message', 'Industry has been added successfully'); 
           \Session::flash('alert-class', 'alert-info'); 
 

           

           return redirect('edit_industry/'.$industry->id);
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $industry = Industry::find($id); 
        // Load user/createOrUpdate.blade.php view
        return \View::make('fieldreports.editindustry')->with('industry', $industry);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if(\Input::get('id')){  
             
             $validator = $this->validator(\Input::all());
             if ($validator->fails()) { 
                
                // get the error messages from the validator
                $messages = $validator->messages(); 
                return redirect('fieldreports.edit_industry/'.\Input::get('id'))
                            ->withErrors($validator)
                            ->withInput();
            }
            
            

            $industry = Industry::find(\Input::get('id'));
            $industry->name = \Input::get('name');
            $industry->display_name = \Input::get('display_name');
            $industry->description = \Input::get('description');
            
            if($industry->save()){ 
           \Session::flash('message', 'Industry has been Updated successfully'); 
           \Session::flash('alert-class', 'alert-info'); }

           return \View::make('fieldreports.editindustry')->with('industry',$industry);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $industry = Industry::find($id);
       if($industry->delete()){
          return \Response::json(array(
                        'error' => false,
                        'message' => 'Industry has been deleted Successfully !!!'),
                        200
                    );
       }
    }
    
}