<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Mail;
use Illuminate\Support\Str;

use App\Sector;
use App\BusinessIdea;
use App\Media;
use App\User;
use Response;
use yajra\Datatables\Datatables;

class BusinessIdeaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $ideas = BusinessIdea::where('status', '=', "Active")->paginate(5);
        $ideas->setPath('businessideas/');
        $sectors = Sector::lists('name', 'id');

        return \View::make('ideas.businessideas', compact('sectors', 'ideas'));
    }

        public function getIndex(){ 
          //
            /**
             * Displays datatables front end view
             *
             * @return \Illuminate\View\View
             */
                
            return \View::make('ideas.listbusinessideas');
          }

          /**
             * Process datatables ajax request.
             *
             * @return \Illuminate\Http\JsonResponse
             */
            public function anyData()
            {
                //return Datatables::of(User::select('*'))->make(true);
                $businessideas = BusinessIdea::select(['id', 'category', 'title', 'short_description', 'price', 'approximate_budget', 'location', 'success_probability', 'created_by', 'updated_at']);

                return Datatables::of($businessideas)
                   ->editColumn('updated_at',function($data){
                        return  date('F d, Y', strtotime($data->updated_at));
                     })
                    ->editColumn('category',function($data){
                        if($data->category)
                        return  Sector::find($data->category)->name;
                     })
                     ->editColumn('created_by',function($data){
                        if($data->created_by){
                           $user = User::find($data->created_by);
                           return $user->avatar = ($user->avatar) ? '<img src="'.  $user->avatar .'" class="img-circle" style="height:25px; width:25px;">'.$user->firstname : '<img src="'.  $default_avatar.'" class="img-circle" style="height:25px; width:25px;">'.$user->firstname;
                        }
                     })
                   ->make(true);
            }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
       
        $sectors =   Sector::lists('name', 'id');
        
        return \View::make('ideas.addbusinessidea', compact('sectors'));
    }


    /**
     *   Validate file formates
     *
     **/
     private function createFileAttachmentValidator($file)
    {
            return Validator::make(
                [
                    'attachment' => $file,
                    'extension'  => strtolower($file->getClientOriginalExtension()),
                ],
                [
                    'attachment' => 'required|max:10000',
                    'extension'  => 'required|in:jpg,jpeg,bmp,png,doc,docx,pdf,rtf,xlsx,xls,csv,txt',
                ]
                //$this->validationMessages()
            );          
    }  


    /**
     *   Validate input data
     *
     **/
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'sector'=>'required',
            'title' => 'required|max:255|unique:ideas,title,'.\Input::get('id'),
            'price' => 'required|numeric',
            'approximate_budget' => 'required|numeric',
            'success_probability'=>'required|numeric',
            'segments'=>'required',
            'status'=>'required',
            'short_description'=>'required|max:255',
            'description'=>'required',
            'location'=>'required'
            
            
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\Request $request)
    {
        //
         
        $validator = $this->Validator(\Input::all());
         if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();

            return redirect('add_idea')
                        ->withErrors($validator)
                        ->withInput();
        }


           $filename ='';
         if (\Request::hasFile('image')) 
            {
                $file = \Request::file('image');
                $image_name = time()."-".$file->getClientOriginalName();
                $file->move('uploads/ideas/image/', $image_name);
                // $image = \Image::make(sprintf('uploads/avtar/%s', $image_name))->resize('160','160')->save('uploads/avtar/resized/'.$image_name);
                // $this->avtar = 'uploads/avtar/'.$image->basename;
                $filename = 'uploads/ideas/image/'.$image_name;
            }
        //$contents = \Storage::get($filename); dd($contents);
        $input = \Input::all();
        
          if(isset($input['segments']))
            $segments = serialize($input['segments']);
       
        
          $idea = BusinessIdea::create([
                                  'category'=>$input['sector'],
                                  'title'=>$input['title'], 
                                  'price'=>$input['price'], 
                                  'short_description'=>$input['short_description'], 
                                  'description'=>$input['description'], 
                                  'segments'=>$segments, 
                                  'success_probability'=>$input['success_probability'], 
                                  'approximate_budget'=>$input['approximate_budget'], 
                                  'image'=>($filename) ? $filename : '' , 
                                  'status'=>$input['status'],
                                  'location'=>$input['location'],
                                  'created_by'=>\Auth::user()->id
                                ]);
        $files = \Request::file('idea_files');
        
        $fileList = array(); 

       if($files[0]){
              foreach ($files as  $file) {
                  # code...
                $validateFile = $this->createFileAttachmentValidator($file);
                if($validateFile->fails()){
                     return redirect('add_idea')
                            ->withErrors($validateFile)
                            ->withInput();
                 }else{
                    $image_name = time()."-".$file->getClientOriginalName();
                    $res = $file->move('uploads/ideas/', $image_name); 
                    
                    $fileList[] = array('name'=>$image_name, 'path'=>'uploads/idea/'.$image_name);
                    Media::create([
                              'belongsto'=>'ideas',
                              'refid'=>$idea->id,
                              'image'=>$image_name,
                              'path'=>'uploads/ideas/'.$image_name,
                              'key'=>Str::random(32),
                            ]);
                 }
              }
          }    
            
          

         if($idea){
             \Session::flash('message', 'Idea has been added successfully'); 
             \Session::flash('alert-class', 'alert-info'); 
             return redirect('edit_idea/'.$idea->id);
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $idea = BusinessIdea::find($id);
        $sectors = Sector::lists('name', 'id');
        $files = \DB::table('media')
                            ->where('refid', '=', $id)
                            ->where('belongsto', '=', 'ideas')
                            ->get();

                 

        
        return \View::make('ideas.editbusinessidea', compact('sectors', 'idea','files'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $validator = $this->Validator(\Input::all());
         if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();

            return redirect('edit_idea/'.\Input::get('id'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $files = \Request::file('idea_files');        
        $fileList = array(); 
      
      

            
          $filename ='';
           if(\Request::file('image')){
             
           if (\Request::hasFile('image')) 
            {
                $file = \Request::file('image');
                $image_name = time()."-".$file->getClientOriginalName();
                $file->move('uploads/ideas/image/', $image_name);
                // $image = \Image::make(sprintf('uploads/avtar/%s', $image_name))->resize('160','160')->save('uploads/avtar/resized/'.$image_name);
                // $this->avtar = 'uploads/avtar/'.$image->basename;
                $filename = 'uploads/ideas/image/'.$image_name;
            }
        } 
        //$contents = \Storage::get($filename); dd($contents);
        $input = \Input::all(); 
        
          if(isset($input['segments']))
            $segments = serialize($input['segments']);

          $idea = BusinessIdea::find($input['id']);
          $idea->category=$input['sector'];
          $idea->title=$input['title'];
          $idea->price=$input['price']; 
          $idea->short_description=$input['short_description'];
          $idea->description=$input['description'];
          $idea->segments=$segments;
          $idea->success_probability=$input['success_probability'];
          $idea->approximate_budget=$input['approximate_budget'];
          $idea->image=($filename) ? $filename : $input['previous_image'] ? $input['previous_image']: ''  ;
          $idea->status=$input['status'];
          $idea->location=$input['location'];
                     
            if($files[0]){ 

               

            \DB::table('media')->where('refid', '=',$idea->id)->where('belongsto','=','ideas')->delete();            

              
              foreach ($files as  $file) {
                  # code...
                $validateFile = $this->createFileAttachmentValidator($file);
                if($validateFile->fails()){
                     return redirect('edit_idea'.\Input::get('id'))
                            ->withErrors($validateFile)
                            ->withInput();
                 }else{
                    $image_name = time()."-".$file->getClientOriginalName();
                    $res = $file->move('uploads/ideas/', $image_name); 
                    
                    $fileList[] = array('name'=>$image_name, 'path'=>'uploads/idea/'.$image_name);
                     Media::create([
                              'belongsto'=>'ideas',
                              'refid'=>$idea->id,
                              'image'=>$image_name,
                              'path'=>'uploads/ideas/'.$image_name,
                              'key'=>Str::random(32),
                            ]);
                 }
              }
          }    

          

         if($idea->save()){
             \Session::flash('message', 'Idea has been updated successfully'); 
             \Session::flash('alert-class', 'alert-info'); 
             return redirect('edit_idea/'.$idea->id);
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        \DB::table('media')->where('refid', '=',$id)->where('belongsto','=','ideas')->delete();
         
         $idea = BusinessIdea::find($id);
       if($idea->delete()){
          return \Response::json(array(
                        'error' => false,
                        'message' => 'Idea has been deleted Successfully !!!'),
                        200
                    );
       }

    }


    /**
     *  Create Email listing for the specific idea
     *  @param  int  $id -idea
     *  @return 
     **/

    public function createMailList(Request $request, $id)
    {
        # code...
        $users = User::all();
        $idea = BusinessIdea::find($id); 
        return \View::make('ideas.addmaillist', compact('users','idea'));

    }

    /**
     *  Create Email listing for the specific idea
     *  @param  int  $id -idea
     *  @return 
     **/

    public function storeMailList(Request $request, $id)
    {
        # code...
        $input = \Input::all(); //dd($imput);
        $mail_listing = $input["mail_listing"];

        

        $idea = BusinessIdea::find($id);
        $idea->mail_listing = \Input::get('mail_listing');

        if($idea->save()){
             \Session::flash('message', 'Mail list has been updated successfully'); 
             \Session::flash('alert-class', 'alert-info'); 
             return redirect('idea/'.$idea->id.'/mail_list');

           }
    }

    /**
    * Download the specified resource from filesystem  
    *
    **/
    public function downloadfile($filepath)
    {
        # code...
        return Response::download('uploads/ideas/'.$filepath);
    }



}

