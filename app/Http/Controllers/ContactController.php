<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Contact;
use Mail;
use yajra\Datatables\Datatables;

class ContactController extends Controller
{
   


   public function getIndex(){ 
  //
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
        
    return \View::make('contactlist');
  }

  /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        //return Datatables::of(User::select('*'))->make(true);
        $contact = Contact::select(['id', 'name', 'email', 'phone', 'subject', 'message', 'created_at']);

        return Datatables::of($contact)
           ->editColumn('created_at',function($data){
                return  date('F d, Y', strtotime($data->created_at));
             })
           ->make(true);
    }




   /**
     * Get a validator for an incoming Contact request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function contactvalidator(array $data)
    {
       //dd(\Input::all());
        return Validator::make($data, [
              
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|digits:10',
            'subject' => 'required|max:2055',
            'message' => 'required|max:2055',
        ]);
    }


    protected function create(array $data)
    {  
        return Contact::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'subject' => $data['subject'],
            'message' => $data['message']
        ]);
    }


  public function submitcontact(\Request $request)
    {
      $contactvalidator = $this->contactvalidator(\Input::all());
         if ($contactvalidator->fails()) { 
                        // get the error messages from the validator
            $messages = $contactvalidator->messages();
             return redirect('contact')
                            ->withErrors($contactvalidator)
                            ->withInput();;
          } 
          else
          {
          $contact = $this->create(\Input::all());
          if($contact){
           \Session::flash('message', 'Submitted successfully'); 
           \Session::flash('alert-class', 'alert-info'); 
           
            $emails = ['sivakazaa@gmail.com', 'siva09.uit@gmail.com','info@rajanbusinessideas.com'];

            $data = array('name' => \Input::get('name'),
                          'email' => \Input::get('email'),
                          'phone' => \Input::get('phone'),
                          'subject' =>\Input::get('subject'),
                          'comment' => \Input::get('message')
                          );
            Mail::send('emails.contact', $data, function($message) use ($emails)
            {    
                $message->to($emails)
                        ->from('info@rajanbusinessideas.com', 'Rajan Business Ideas')
                        ->subject('New Contact Information from Rajan Business Ideas');    
            });

            
            return \View::make('contact');
            }//else{ echo 'Failed';}
          }
     }
}
