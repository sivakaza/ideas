<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use App\Http\Controllers\Controller;
use Validator;

use yajra\Datatables\Datatables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(){ 
    //
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
        
        return \View::make('category');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        //return Datatables::of(User::select('*'))->make(true);
        $category = Category::select(['id', 'name', 'display_name', 'description', 'created_at']);

        return Datatables::of($category)
           ->editColumn('created_at',function($data){
                return  date('F d, Y', strtotime($data->created_at));
             })
           ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         return \View::make('addcategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $validator = $this->Validator(\Input::all());
         if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();

            return redirect('add_category')
                        ->withErrors($validator)
                        ->withInput();
        }
       
         $data = \Input::all();
         $category = Category::create(['name' => $data['name'],
                                'display_name' => $data['display_name'],
                                'description' => $data['description']
                                ]); 
          if($category){
           \Session::flash('message', 'Category has been added successfully'); 
           \Session::flash('alert-class', 'alert-info'); 
 

           

           return redirect('edit_category/'.$category->id);
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $category = Category::find($id); 
        // Load user/createOrUpdate.blade.php view
        return \View::make('editcategory')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if(\Input::get('id')){  
             
             $validator = $this->validator(\Input::all());
             if ($validator->fails()) { 
                
                // get the error messages from the validator
                $messages = $validator->messages(); 
                return redirect('edit_category/'.\Input::get('id'))
                            ->withErrors($validator)
                            ->withInput();
            }
            
            

            $category = Category::find(\Input::get('id'));
            $category->name = \Input::get('name');
            $category->display_name = \Input::get('display_name');
            $category->description = \Input::get('description');
            
            if($category->save()){ 
           \Session::flash('message', 'Category has been Updated successfully'); 
           \Session::flash('alert-class', 'alert-info'); }

           return \View::make('editcategory')->with('category',$category);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
