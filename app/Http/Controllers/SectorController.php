<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Sector;
use App\Http\Controllers\Controller;
use Validator;

use yajra\Datatables\Datatables;

class SectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(){ 
    //
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
        
        return \View::make('ideas.sectors');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        //return Datatables::of(User::select('*'))->make(true);
        $sector = Sector::select(['id', 'name', 'display_name', 'description', 'created_at']);

        return Datatables::of($sector)
           ->editColumn('created_at',function($data){
                return  date('F d, Y', strtotime($data->created_at));
             })
           ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         return \View::make('ideas.addsector');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $validator = $this->Validator(\Input::all());
         if ($validator->fails()) { 
            
            // get the error messages from the validator
            $messages = $validator->messages();

            return redirect('ideas.add_sector')
                        ->withErrors($validator)
                        ->withInput();
        }
       
         $data = \Input::all();
         $sector = Sector::create(['name' => $data['name'],
                                'display_name' => $data['display_name'],
                                'description' => $data['description']
                                ]); 
          if($sector){
           \Session::flash('message', 'Sector has been added successfully'); 
           \Session::flash('alert-class', 'alert-info'); 
 

           

           return redirect('edit_sector/'.$sector->id);
         }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:sectors,name,'.\Input::get('id'),
            'display_name' => 'required|max:255',
            
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $sector = Sector::find($id); 
        // Load user/createOrUpdate.blade.php view
        return \View::make('ideas.editsector')->with('sector', $sector);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if(\Input::get('id')){  
             
             $validator = $this->validator(\Input::all());
             if ($validator->fails()) { 
                
                // get the error messages from the validator
                $messages = $validator->messages(); 
                return redirect('edit_Sector/'.\Input::get('id'))
                            ->withErrors($validator)
                            ->withInput();
            }
            
            

            $sector = Sector::find(\Input::get('id'));
            $sector->name = \Input::get('name');
            $sector->display_name = \Input::get('display_name');
            $sector->description = \Input::get('description');
            
            if($sector->save()){ 
           \Session::flash('message', 'Sector has been Updated successfully'); 
           \Session::flash('alert-class', 'alert-info'); }

           return \View::make('ideas/editsector')->with('sector',$sector);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sector = Sector::find($id);
       if($sector->delete()){
          return \Response::json(array(
                        'error' => false,
                        'message' => 'Sector has been deleted Successfully !!!'),
                        200
                    );
       }
    }
}
