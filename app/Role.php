<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name', 'display_name', 'description', 'created_at', 'updated_at'];

    /*
     *  Assigned roles
     *  @returns : all Roles of a specific user
     **/
    public function permissions()
    {
        return $this->belongsToMany('App\Permission','permission_role');
    }

}
