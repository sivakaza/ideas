<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'belongsto', 'refid', 'image', 'path', 'key'];

}
